package com.app.mthandenimanqele.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.ui.ViewImage;

import java.util.List;

public class Photos_RecycleView_Adapter extends RecyclerView.Adapter<Photos_RecycleView_Adapter.MyViewHolder> {

    Context context;
    private List<String> collectionModalClasses;




    public Photos_RecycleView_Adapter(Context context, List<String> listModalClassList) {
        this.collectionModalClasses = listModalClassList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_collection_profile2, parent, false);
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position){
        String imageurl = collectionModalClasses.get(position);
            Glide.with(context)
                  .load(imageurl)
                  .disallowHardwareConfig()
                  .into(holder.image);
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, ViewImage.class).putExtra("url",imageurl));
                }
            });
    }

    @Override
    public int getItemCount() {
        return collectionModalClasses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        ImageView image;

        public MyViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.image);
        }

    }
}
