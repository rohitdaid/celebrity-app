package com.app.mthandenimanqele.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.app.mthandenimanqele.R;

import java.util.List;

public class Listall_videos_RecyclerView_Adapter extends RecyclerView.Adapter<Listall_videos_RecyclerView_Adapter.MyViewHolder> {

    Context context;
    private List<String> postLikeModalClasses;
    WebView webview;
    private String videothumbnailurl="https://img.youtube.com/vi/";


    public Listall_videos_RecyclerView_Adapter(Context mainActivityContacts, List<String> listModalClassList) {
        this.postLikeModalClasses = listModalClassList;
        this.context = mainActivityContacts;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.all_videos_items_list, parent, false);



        return new MyViewHolder(itemView);


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position){
        String videourl = postLikeModalClasses.get(position);
        String[] videourlsplit = videourl.split("=");
        String videothumbnail0frame = videourlsplit[1];
        String videothumbnailpath=videothumbnailurl+videothumbnail0frame+"/0.jpg";
        Log.d("thumbnailpath ",videothumbnailpath);
        Glide.with(context)
                .load(videothumbnailpath)
                .disallowHardwareConfig()
                .into(holder.like_image);
        holder.like_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(videourl)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return postLikeModalClasses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        ImageView like_image;

        public MyViewHolder(View view) {
            super(view);


            like_image =  view.findViewById(R.id.like_image);


        }

    }
}
