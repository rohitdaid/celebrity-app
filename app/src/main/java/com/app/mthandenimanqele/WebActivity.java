package com.app.mthandenimanqele;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class WebActivity extends AppCompatActivity implements View.OnClickListener {

    private WebView mWebView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        mWebView = findViewById(R.id.webview);
        Button closeVIEW = findViewById(R.id.button);
        closeVIEW.setOnClickListener(this);
        String value = getIntent().getExtras().getString("URL");
        mWebView.loadUrl(value);
        mWebView.getSettings().setJavaScriptEnabled(true);
    }
    @Override
    public void onClick(View v) {
        onBackPressed();
        finish();
    }


    @Override
    public void onBackPressed() {
        //this is only needed if you have specific things
        //that you want to do when the user presses the back button.
        /* your specific things...*/
        super.onBackPressed();
        finish();
    }
}