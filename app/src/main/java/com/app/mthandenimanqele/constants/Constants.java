package com.app.mthandenimanqele.constants;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.app.mthandenimanqele.R;

public class Constants {
    public interface ACTION {
        public static String MAIN_ACTION = "com.ontrack.mthandenimanqele.constants.action.main";
        public static String INIT_ACTION = "com.ontrack.mthandenimanqele.constants.action.init";
        public static String PREV_ACTION = "com.ontrack.mthandenimanqele.constants.action.prev";
        public static String PLAY_ACTION = "ACTION_PLAY";
        public static String PAUSE_ACTION="ACTION_PAUSE";
        public static String NEXT_ACTION = "com.ontrack.mthandenimanqele.constants.action.next";
        public static String STARTFOREGROUND_ACTION = "com.ontrack.mthandenimanqele.constants.action.startforeground";
        public static String STOPFOREGROUND_ACTION = "com.ontrack.mthandenimanqele.constants.action.stopforeground";

    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 1001;
    }

    public static Bitmap getDefaultAlbumArt(Context context) {
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            bm = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.mm, options);
        } catch (Error ee) {
        } catch (Exception e) {
        }
        return bm;
    }
}
