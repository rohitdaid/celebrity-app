package com.app.mthandenimanqele.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.webservices.ApiService;
import com.app.mthandenimanqele.webservices.RetrofitClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyOtpActivity extends AppCompatActivity {

    Button verifyotp;
    @BindView(R.id.et_code)
    EditText verification_code;
    String code,email;
    ProgressDialog progressDialog;
    private String TAG=getClass().getName();
    @BindView(R.id.resend_code)
    TextView resend_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        ButterKnife.bind(this);
        verifyotp=findViewById(R.id.btn_login);
        progressDialog=new ProgressDialog(this);
        Intent i=getIntent();
        email=i.getStringExtra("email");
        resend_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotpassword(email);
            }
        });
        verifyotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                code=verification_code.getText().toString();
                if(code.isEmpty()){
                    Toasty.warning(getApplicationContext(),"Enter verification code",Toasty.LENGTH_SHORT).show();
                }else{
                    verifycode(email,code);
                }
            }
        });
    }

    private void forgotpassword(String email) {
        progressDialog.show();
        ApiService apiService= RetrofitClient.getClient().create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("email", email);
            RequestBody body = RequestBody.create(MediaType.parse("text/plain"), String.valueOf((paramObject)));
            Call<ResponseBody> call=apiService.forgetPassword(body);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful() && response.code()==200){
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject=new JSONObject(response.body().string());
                            String message=jsonObject.getString("message");
                            Toasty.success(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), VerifyOtpActivity.class)
                                    .putExtra("email",email));
                            //customType(ForgotPassActivity.this, "right-to-left");
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else if(response.code()==422){
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            JSONObject errorObject = jsonObject.getJSONObject("errors");
                            String message = errorObject.getString("email");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                    else{
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            String message = jsonObject.getString("error_description");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d(TAG,"error "+t.getMessage());
                    Toasty.error(VerifyOtpActivity.this, t.getLocalizedMessage(), Toasty.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void verifycode(String email,String code) {
        progressDialog.show();
        ApiService apiService= RetrofitClient.getClient().create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("email", email);
            paramObject.put("code",code);
            RequestBody body = RequestBody.create(MediaType.parse("text/plain"), String.valueOf((paramObject)));
            Call<ResponseBody> call=apiService.verifyotp(body);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful() && response.code()==200){
                        try {
                            JSONObject jsonObject=new JSONObject(response.body().string());
                            String message=jsonObject.getString("message");
                            JSONObject dataObject=jsonObject.getJSONObject("data");
                            String verify_token=dataObject.getString("token");
                            Toasty.success(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), ResetPassActivity.class)
                                    .putExtra("email",email)
                                    .putExtra("token",verify_token));
                            //customType(VerifyOTPActivity.this, "right-to-left");
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }else if(response.code()==404) {
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            String message = jsonObject.getString("message");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }else{
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            String message = jsonObject.getString("error");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d(TAG,"error "+t.getMessage());
                    progressDialog.dismiss();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
