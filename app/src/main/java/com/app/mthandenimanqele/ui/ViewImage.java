package com.app.mthandenimanqele.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.app.mthandenimanqele.R;
import com.tonyodev.fetch2.AbstractFetchListener;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2core.DownloadBlock;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Random;

import es.dmoral.toasty.Toasty;
import pub.devrel.easypermissions.EasyPermissions;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ViewImage extends AppCompatActivity  {

    ImageView image, back, download;
    private String url;
    private String errormsg;
    private String TAG="ViewImageClass";
    private Fetch fetch;
    private String folderName = "MthandeniManqele/Images";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);
        image = findViewById(R.id.image);
        back = findViewById(R.id.back);
        download = findViewById(R.id.download);
        Intent i = getIntent();
        url = i.getStringExtra("url");
        Glide.with(getApplicationContext())
                .load(url)
                .into(image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // downloadfile(url);
                checkStoragePermission();
            }
        });
    }

    private void checkStoragePermission() {
        if (ActivityCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Check permission
            ActivityCompat.requestPermissions(this,
                    new String[]{WRITE_EXTERNAL_STORAGE},
                    1);
            Log.d(TAG,"storage permission required");
        } else {
            startDownloadImageInBackground(url);
        }

    }

    public void downloadfile(String imageurl) {
        new DownloadFile().execute(imageurl);
        Log.d("errormsg", "downloadfile: "+errormsg);
    }

    public class DownloadFile extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;
        public String storagepath;
        final Random generator = new Random();
        int n = 10000;
        private String isdownlaodsuccess="false";


        @Override
        protected String doInBackground(String... strings) {
            int count;
            String chkfiletype = strings[0];
            File storageDir = null;
            String[] fileurl = strings[0].split("media/");
            String fname = fileurl[1];
            Log.d("urll", "doInBackground: " + strings[0]+fname);

            try {
                URL url = new URL(strings[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lengthOfFile = connection.getContentLength();


                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);


                n = generator.nextInt(n);

                //Extract file name from URL
                // fileName = strings[0].substring(strings[0].lastIndexOf('/') + 1, strings[0].length());

                //Append timestamp to file name



                    //fname = "IMG" + n + ".jpg";
                    fileName = fname;
                    //External directory path to save file
                    // folder = Environment.getExternalStorageDirectory() + File.separator + "chatappfile/";
                    storageDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/MthandeniManqele/Image");

                boolean success = true;
                assert storageDir != null;
                if (!storageDir.exists()) {
                    storageDir.mkdirs();
                    success = storageDir.mkdirs();
                }
                if (success) {
                    File imageFile = new File(storageDir, fileName);
                    // Output stream to write file
                    OutputStream output = new FileOutputStream(imageFile);

                    byte data[] = new byte[1024];

                    long total = 0;

                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
                        publishProgress("" + (int) ((total * 100) / lengthOfFile));
                        Log.d("context", "Progress: " + (int) ((total * 100) / lengthOfFile));

                        // writing data to file
                        output.write(data, 0, count);
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();
                    storagepath = imageFile + fileName;
                    Log.d("storagepath", "doInBackground: " + storagepath);

                    if (storagepath != null || !storagepath.equals("")) {
                        isdownlaodsuccess = "true";
                        //oTchat_chatDatabase.getmessage(OTchat_Chat_View.selecteduserid);
                    } else {
                        isdownlaodsuccess = "false";
                    }
                        return null;
                }
            } catch (Exception e) {
                Log.d("Error: ", "error" + e.getMessage());
                errormsg = "Something went wrong";
            }

            return errormsg;
        }


    }

    private void startDownloadImageInBackground(String imageurl) {
        Log.d(TAG, "startDownloadImageInBackground: called message:"+imageurl);
        FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(this)
                .setDownloadConcurrentLimit(3)
                .enableLogging(true)
                .createDownloadFileOnEnqueue(false)
                .setProgressReportingInterval(300)
                .build();
        fetch = Fetch.Impl.getInstance(fetchConfiguration);
        fetch.addListener(fetchListener);
        String[] fileurl = imageurl.split("media/");
        String fileName = fileurl[1];
        String url = imageurl;

        String file = "/storage/emulated/0/"+folderName + "/" + fileName;
        Log.v("PROG-D", "fileName: " + fileName);
        Log.v("PROG-D", "url: " + url);
        Log.v("PROG-D", "file: " + file);

        final Request request = new Request(url, file);
        request.setTag("download");
        request.setNetworkType(NetworkType.ALL);
        request.setPriority(Priority.HIGH);
        fetch.enqueue(request, updatedRequest -> {
            //Request was successfully enqueued for download.
            Toast.makeText(this, "Downloading", Toast.LENGTH_SHORT).show();

            // AppDb.getInstance(activity)
            //       .chatDao().updateIsDownloaded("abc.com",true, Integer.parseInt(MessageId));
        }, error -> {
            //An error occurred enqueuing the request.
            Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();

        });
    }

    private FetchListener fetchListener = new AbstractFetchListener() {
        @Override
        public void onProgress(@NotNull Download download, long etaInMilliSeconds, long downloadedBytesPerSecond) {
            super.onProgress(download, etaInMilliSeconds, downloadedBytesPerSecond);
            Log.d("TestActivity", "Progress: " + download.getProgress()+":"+download.getStatus());

            /*progressDialog = new ProgressDialog(ChatSupportActivity.this);
            //Set the progress dialog to display a horizontal progress bar
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            //Set the dialog title to 'Loading...'
            progressDialog.setTitle("Downloading...");
            //Set the dialog message to 'Loading application View, please wait...'
            progressDialog.setMessage("Loading application View, please wait...");
            //This dialog can't be canceled by pressing the back key
            progressDialog.setCancelable(false);
            //This dialog isn't indeterminate
            progressDialog.setIndeterminate(false);
            //The maximum number of items is 100
            progressDialog.setMax(100);
            //Set the current progress to zero
            progressDialog.setProgress(download.getProgress());
            progressDialog.incrementProgressBy(10);
            //Display the progress dialog
            progressDialog.show();*/

        }

        @Override
        public void onPaused(@NotNull Download download) {
            super.onPaused(download);
            //progressDialog.dismiss();
        }

        @Override
        public void onAdded(@NotNull Download download) {
            super.onAdded(download);
            Log.d("TestActivity", "progersss: "+download.getStatus());

        }
        @Override
        public void onDownloadBlockUpdated(@NotNull Download download, @NotNull DownloadBlock downloadBlock, int totalBlocks) {
            super.onDownloadBlockUpdated(download, downloadBlock, totalBlocks);
            Log.d("TestActivity", "downloadblock "+downloadBlock+":"+download.getStatus()+"\n"+download.getProgress());
        }

        @Override
        public void onError(@NotNull Download download, @NotNull Error error, @Nullable Throwable throwable) {
            super.onError(download, error, throwable);
            Log.d("TestActivity", "Completed "+error);
            // progressDialog.dismiss();

        }

        @Override
        public void onStarted(@NotNull Download download, @NotNull List<? extends DownloadBlock> downloadBlocks, int totalBlocks) {
            super.onStarted(download, downloadBlocks, totalBlocks);
            Log.d("testacrivy", "onStarted: "+download.getProgress());
        }

        @Override
        public void onCompleted(@NotNull Download download) {
            super.onCompleted(download);
            Log.d("TestActivity", "Completed "+download.getFileUri());
            String Messages = download.getFileUri().toString();
            String parts[] = Messages.split("file://");
            Log.d("TEST", "onCompleted:  parts:"+parts[1]);
            Toasty.success(getApplicationContext(),"Image Downloaded at "+folderName,Toasty.LENGTH_SHORT).show();
            //progressDialog.dismiss();
            fetch.close();


        }
    };
}
