package com.app.mthandenimanqele.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.utils.Pref;
import com.app.mthandenimanqele.webservices.ApiService;
import com.app.mthandenimanqele.webservices.RetrofitClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {



    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_password)
    EditText et_password;

    String email,password;

    @BindView(R.id.btn_login)
    Button btnlogin;
    private String TAG=getClass().getName();
    ProgressDialog progressDialog;
    private String message;

    @OnClick(R.id.btn_login)
    public void Loginclick(){
        email=et_email.getText().toString();
        password=et_password.getText().toString();
        if(email.isEmpty()){
            Toasty.warning(getApplicationContext(),"Enter email",Toasty.LENGTH_SHORT).show();
        }if(password.isEmpty()){
            Toasty.warning(getApplicationContext(),"Enter password",Toasty.LENGTH_SHORT).show();
        }else{
            login(email,password,"abcd");
        }
    }

    @BindView(R.id.tv_to_register)
    TextView register;
    @OnClick(R.id.tv_to_register)
    public void Registerclick(){
        startActivity(new Intent(this,RegisterActivity.class));
        finish();
    }

    @BindView(R.id.to_forgot_password)
    TextView forgot_password;
    @OnClick(R.id.to_forgot_password)
    public void Forgotpasswordclick(){
        startActivity(new Intent(this,ForgotPassActivity.class));
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        progressDialog=new ProgressDialog(this);
    }


    private void login(String email, String password,String deviceToken) {
        Log.d(TAG, "login: token:"+deviceToken);
        //progressDialog.show();
        progressDialog.show();
        ApiService apiService = RetrofitClient.getClient().create(ApiService.class);

        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("email", email);
            paramObject.put("password", password);
            paramObject.put("deviceType", "Android");
            paramObject.put("deviceToken", deviceToken);
            RequestBody body = RequestBody.create(MediaType.parse("text/plain"), String.valueOf((paramObject)));
            Call<ResponseBody> call=apiService.
                    login(body);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful() && response.code()==200){
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject=new JSONObject(response.body().string());
                            message=jsonObject.getString("message");
                            JSONObject dataobject=jsonObject.getJSONObject("data");
                            String token=dataobject.getString("token");
                            String user_id =dataobject.getString("user_id");
                            String first_name = dataobject.getString("first_name");
                            String last_name = dataobject.getString("last_name");
                            String email = dataobject.getString("email");

                            Log.d(TAG,"token value "+token+"\n"+"name:"+first_name+" "+last_name +"id:"+user_id+" email:"+email);
                            Pref.putUserToken(getApplicationContext(),token);
                            /*Pref.putUserId(MyApp.getContext(),user_id);
                            Pref.putUserName(MyApp.getContext(),first_name+" "+last_name);
                            Pref.putUserEmail(MyApp.getContext(),email);*/
                            Toasty.success(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), BioActivity.class));
                            //customType(LoginActivity.this, "right-to-left");
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else if(response.code()==422){
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            message = jsonObject.getString("error_description");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            message = jsonObject.getString("error");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d(TAG,"error "+t.getMessage());
                    message = t.getLocalizedMessage();
                    Toast.makeText(LoginActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
