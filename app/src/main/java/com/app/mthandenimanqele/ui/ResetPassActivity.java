package com.app.mthandenimanqele.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.webservices.ApiService;
import com.app.mthandenimanqele.webservices.RetrofitClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPassActivity extends AppCompatActivity {

    Button resetbtn;
    @BindView(R.id.et_newpswrd)
    EditText et_newpswrd;
    @BindView(R.id.et_cnfrmpswrd)
    EditText et_cnfrmpswrd;

    String newpswrd,cnfrmpswrd,email,token,message;
    private String TAG=getClass().getName();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pass);
        ButterKnife.bind(this);
        resetbtn=findViewById(R.id.btn_reset);
        progressDialog=new ProgressDialog(this);
        Intent i=getIntent();
        email=i.getStringExtra("email");
        token=i.getStringExtra("token");
        resetbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int error = 0;
                newpswrd=et_newpswrd.getText().toString();
                cnfrmpswrd=et_cnfrmpswrd.getText().toString();
                if(newpswrd.isEmpty()){
                    error=1;
                    Toasty.warning(getApplicationContext(),"Enter Password",Toasty.LENGTH_SHORT).show();
                }if(cnfrmpswrd.isEmpty()){
                    error=1;
                    Toasty.warning(getApplicationContext(),"Enter Password",Toasty.LENGTH_SHORT).show();
                }if(!newpswrd.equals(cnfrmpswrd)){
                    error=1;
                    Toasty.warning(getApplicationContext(),"Password Doesn't Match",Toasty.LENGTH_SHORT).show();
                }else{
                    if(error>0){ }
                    else {
                        resetPassword(newpswrd,cnfrmpswrd,token,email);
                    }
                }
            }
        });
    }

    private void resetPassword(String newpswrd, String cnfrmpswrd, String token, String email) {
        progressDialog.show();
        ApiService apiService= RetrofitClient.getClient().create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("email", email);
            paramObject.put("token",token);
            paramObject.put("newPassword",newpswrd);
            paramObject.put("confirmedPassword",cnfrmpswrd);
            RequestBody body = RequestBody.create(MediaType.parse("text/plain"), String.valueOf((paramObject)));
            Call<ResponseBody> call=apiService.resetpassword(body);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful() && response.code()==200){
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response.body().string());
                            String message=jsonObject.getString("message");
                            Toasty.success(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            //customType(ResetPassActivity.this, "bottom-to-up");
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else if(response.code()==404) {
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            String message = jsonObject.getString("message");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                    else{
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            String message = jsonObject.getString("error_description");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d(TAG,"error "+t.getMessage());
                    progressDialog.dismiss();
                    message = t.getLocalizedMessage();
                    Toasty.error(ResetPassActivity.this, t.getLocalizedMessage(), Toasty.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
