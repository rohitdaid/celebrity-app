package com.app.mthandenimanqele.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.mthandenimanqele.NetworkError;
import com.app.mthandenimanqele.NetworkStateReceiver;
import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.utils.Pref;
import com.app.mthandenimanqele.webservices.ApiService;
import com.app.mthandenimanqele.webservices.RetrofitClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, NetworkStateReceiver.NetworkStateReceiverListener {

    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.editprofile)
    ImageView editprofile;
    @BindView(R.id.insta)
    ImageView insta;
    @BindView(R.id.facebook)
    ImageView facebook;
    @BindView(R.id.twitter)
    ImageView twitter;
    @BindView(R.id.tvName)
    TextView name;
    @BindView(R.id.tvEmail)
    TextView txtemail;
    @BindView(R.id.tvdob)
    TextView tvdob;
    @BindView(R.id.tvBorn)
    TextView tvBorn;
    @BindView(R.id.bornll)
    LinearLayout bornll;
    @BindView(R.id.sociaaccount)
    TextView sociaaccount;
    @BindView(R.id.sociall)
    LinearLayout sociaaccountll;
    @BindView(R.id.insta_ll)
    LinearLayout insta_ll;
    @BindView(R.id.fb_ll)
    LinearLayout fb_ll;
    @BindView(R.id.twitter_ll)
    LinearLayout twitter_ll;
    @BindView(R.id.profile_icon)
    CircularImageView profile_icon;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        progressDialog=new ProgressDialog(this);
        ButterKnife.bind(this);
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        back.setOnClickListener(this);
        editprofile.setOnClickListener(this);
        insta.setOnClickListener(this);
        facebook.setOnClickListener(this);
        twitter.setOnClickListener(this);
        fetchprofile();
        checksocialaccounts();

    }

    private void checksocialaccounts() {
        /*if(Pref.getUserFb(getApplicationContext())==null || Pref.getUserFb(getApplicationContext()).equals("")){
            fb_ll.setVisibility(View.GONE);
        }
        else if(Pref.getUserInstagram(getApplicationContext())==null || Pref.getUserInstagram(getApplicationContext()).equals("")){
            insta_ll.setVisibility(View.GONE);
        }
        else if(Pref.getUserTwitter(getApplicationContext())==null || Pref.getUserTwitter(getApplicationContext()).equals("")){
            twitter_ll.setVisibility(View.GONE);
        }else if(Pref.getUserFb(getApplicationContext())==null || Pref.getUserFb(getApplicationContext()).equals("")
                && Pref.getUserInstagram(getApplicationContext())==null || Pref.getUserInstagram(getApplicationContext()).equals("")
        && Pref.getUserTwitter(getApplicationContext())==null || Pref.getUserTwitter(getApplicationContext()).equals("")){
            sociaaccount.setVisibility(View.VISIBLE);
        }
        else if(Pref.getUserFb(getApplicationContext())!=null && !Pref.getUserFb(getApplicationContext()).equals("")
                || Pref.getUserInstagram(getApplicationContext())!=null && !Pref.getUserInstagram(getApplicationContext()).equals("")
                || Pref.getUserTwitter(getApplicationContext())!=null || !Pref.getUserTwitter(getApplicationContext()).equals("")){
            sociaaccountll.setVisibility(View.VISIBLE);
            sociaaccount.setVisibility(View.GONE);
        }*/
        if(Pref.getUserInstagram(getApplicationContext())!=null  ){
            if(!Pref.getUserInstagram(getApplicationContext()).isEmpty() && !Pref.getUserInstagram(getApplicationContext()).equals("null")){
                sociaaccountll.setVisibility(View.VISIBLE);
                sociaaccount.setVisibility(View.GONE);
                insta_ll.setVisibility(View.VISIBLE);
            }
        }
        if(Pref.getUserTwitter(getApplicationContext())!=null ){
            if(!Pref.getUserTwitter(getApplicationContext()).isEmpty() && !Pref.getUserTwitter(getApplicationContext()).equals("null"))
            {
                sociaaccountll.setVisibility(View.VISIBLE);
                sociaaccount.setVisibility(View.GONE);
                twitter_ll.setVisibility(View.VISIBLE);
            }
        } if(Pref.getUserFb(getApplicationContext())!=null) {
            if (!Pref.getUserFb(getApplicationContext()).isEmpty() && !Pref.getUserFb(getApplicationContext()).equals("null") ) {
                sociaaccountll.setVisibility(View.VISIBLE);
                sociaaccount.setVisibility(View.GONE);
                fb_ll.setVisibility(View.VISIBLE);
            }
        }
        Log.d("accounts","insta "+Pref.getUserInstagram(getApplicationContext())+"\n"+
                "fb "+Pref.getUserFb(getApplicationContext())+"\n"+
                "twitter "+Pref.getUserTwitter(getApplicationContext()));
    }

    private void fetchprofile() {
        progressDialog.show();
        ApiService apiService= RetrofitClient.getClient().create(ApiService.class);
        Call<ResponseBody> call=apiService.profile(Pref.getUserToken(getApplicationContext()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful() && response.code()==200){
                    try {
                        JSONObject jsonObject=new JSONObject(response.body().string());
                        JSONObject dataObject=jsonObject.getJSONObject("data");
                        String fname=dataObject.getString("first_name");
                        String lname=dataObject.getString("last_name");
                        String email=dataObject.getString("email");
                        String profile=dataObject.getString("avatar");
                        String mobile=dataObject.getString("mobile");
                        String dob=dataObject.getString("dob");
                        String address=dataObject.getString("address");
                        String avtar=dataObject.getString("avtar");
                        String branch=dataObject.getString("branch");
                        String province=dataObject.getString("province");
                        String induna_name=dataObject.getString("induna_name");
                        JSONObject metaDataObject=dataObject.getJSONObject("meta");
                        String about_igcokama=metaDataObject.getString("about_igcokama");
                        String preference=metaDataObject.getString("preference");
                        String facebook_link=metaDataObject.getString("facebook_link");
                        String twitter_link=metaDataObject.getString("twitter_link");
                        String instagram_link=metaDataObject.getString("instagram_link");
                        Log.d("Profile_Activity ","profile "+about_igcokama);

                        String[] datesplit = dob.split(" ");
                        String date = datesplit[0];
                        String[] dateofbirth = date.split("-");
                        String month= dateofbirth[1];
                        Pref.putUserEmail(getApplicationContext(),email);
                        Pref.putUserFirstname(getApplicationContext(),fname);
                        Pref.putUserLastname(getApplicationContext(),lname);
                        Pref.putUserMobile(getApplicationContext(),mobile);
                        Pref.putInduna_Name(getApplicationContext(),induna_name);
                        Pref.putAbout_Igcokama(getApplicationContext(),about_igcokama);
                        Pref.putUSER_Address(getApplicationContext(),address);
                        Pref.putUSER_PROVINCE(getApplicationContext(),province);
                        Pref.putUSER_DP(getApplicationContext(),avtar);
                        Pref.putUSER_BRANCH(getApplicationContext(),branch);
                        Pref.putUSER_preference(getApplicationContext(),preference);
                        Pref.putUserInstagram(getApplicationContext(),instagram_link);
                        Pref.putUserFb(getApplicationContext(),facebook_link);
                        Pref.putUserTwitter(getApplicationContext(),twitter_link);
                        String savedate=dateofbirth[0]+"-"+month+"-"+dateofbirth[2];
                        if(Integer.valueOf(month)!=0){
                            int monthindex=Integer.valueOf(month)-1;
                            date= dateofbirth[2];
                            String year= dateofbirth[0];
                            Resources resource=getResources();
                            String[] months=resource.getStringArray(R.array.Months);
                            month=months[monthindex];
                            tvdob.setText(date+" "+month+" "+year);
                            Pref.putUserDOB(getApplicationContext(),savedate);
                        }else{
                            tvBorn.setVisibility(View.GONE);
                            bornll.setVisibility(View.GONE);
                        }
                        Glide.with(getApplicationContext())
                                .load(profile)
                                .placeholder(R.drawable.ic_account_circle_black_24dp)
                                .into(profile_icon);
                        name.setText(fname+" "+lname);
                        txtemail.setText(email);
                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if(response.code()==401){
                    try {
                        JSONObject jsonObject=new JSONObject(response.errorBody().string());
                        String msg= jsonObject.getString("message");
                        Toasty.warning(getApplicationContext(),msg,Toasty.LENGTH_SHORT).show();
                        Pref.clearPref(getApplicationContext());
                        startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.imgBack){
            finish();
        }if(v.getId()==R.id.editprofile){
            startActivity(new Intent(getApplicationContext(),EditProfile.class));
        }if(v.getId()==R.id.insta){
            try{
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Pref.getUserInstagram(getApplicationContext()))));
            }catch (ActivityNotFoundException e){
                Toasty.warning(getApplicationContext(),"Enter valid profile link",Toasty.LENGTH_SHORT).show();
            }
        }if(v.getId()==R.id.facebook){
            try{
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Pref.getUserFb(getApplicationContext()))));
            }catch (ActivityNotFoundException e){
                Toasty.warning(getApplicationContext(),"Enter valid profile link",Toasty.LENGTH_SHORT).show();
            }
        }if(v.getId()==R.id.twitter){
            try{
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Pref.getUserTwitter(getApplicationContext()))));
            }catch (ActivityNotFoundException e){
                Toasty.warning(getApplicationContext(),"Enter valid profile link",Toasty.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchprofile();
        checksocialaccounts();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {
        Intent i=new Intent(this, NetworkError.class);
        startActivity(i);
    }
}
