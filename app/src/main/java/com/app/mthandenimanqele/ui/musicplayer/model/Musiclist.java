package com.app.mthandenimanqele.ui.musicplayer.model;

import java.util.List;

public class Musiclist {

    private List<String> music;
    private List<String> artistname;
    private List<String> albumart;

    public List<String> getMusic() {
        return music;
    }

    public void setMusic(List<String> music) {
        this.music = music;
    }

    public List<String> getArtistname() {
        return artistname;
    }

    public void setArtistname(List<String> artistname) {
        this.artistname = artistname;
    }

    public List<String> getAlbumart() {
        return albumart;
    }

    public void setAlbumart(List<String> albumart) {
        this.albumart = albumart;
    }
}
