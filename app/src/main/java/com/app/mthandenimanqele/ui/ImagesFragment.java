package com.app.mthandenimanqele.ui;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.adapter.Photos_RecycleView_Adapter;
import com.app.mthandenimanqele.webservices.BaseUrl;

import java.util.ArrayList;
import java.util.List;

public class ImagesFragment extends Fragment {
    List<String> imageslist=new ArrayList<>();
    Resources res;
    RecyclerView recycler_view;
    Photos_RecycleView_Adapter photos_recycleView_adapter;
    public ImagesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment\
        res=getResources();
        View view=inflater.inflate(R.layout.fragment_images,container,false);
        recycler_view=view.findViewById(R.id.recycler_view);
        String[] images=res.getStringArray(R.array.images);
        for(int i=0;i<images.length;i++){
            imageslist.add(i, BaseUrl.imageurl+images[i]);
        }
        Log.d("imagesfragment ","imageslist "+imageslist.toString());
        photos_recycleView_adapter=new Photos_RecycleView_Adapter(getContext(),imageslist);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recycler_view.setLayoutManager(layoutManager1);
        recycler_view.setAdapter(photos_recycleView_adapter);
        return view;
    }
}
