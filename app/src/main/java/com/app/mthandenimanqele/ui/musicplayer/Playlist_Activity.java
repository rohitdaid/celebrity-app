package com.app.mthandenimanqele.ui.musicplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.app.mthandenimanqele.NetworkError;
import com.app.mthandenimanqele.NetworkStateReceiver;
import com.app.mthandenimanqele.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Playlist_Activity extends AppCompatActivity implements View.OnClickListener, NetworkStateReceiver.NetworkStateReceiverListener  {

    @BindView(R.id.playlist_rv)
    RecyclerView playlistrv;
    @BindView(R.id.back)
    ImageView back;
    List<String> musiclist;
    List<String> musicnamelist;
    List<String> artistamelist;
    Playlist_Adapter playlist_adapter;
    String[ ] songs,songsname,artistnames;
    private MusicPlayer musciplayer;
    NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);
        ButterKnife.bind(this);
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        back.setOnClickListener(this);
        Resources res = getResources();
        /*songs=res.getStringArray(R.array.songs);
        songsname=res.getStringArray(R.array.songsname);
        artistnames=res.getStringArray(R.array.artistname);*/
        musiclist=new ArrayList<>();
        artistamelist=new ArrayList<>();
        musicnamelist=new ArrayList<>();

       // Log.d("selected_musicitem","url "+audioIndex);
        /*musiclist.add("http://13.233.184.187/Celebrity/music/Ankhiyan_De_Nede _JordanSandhu.mp3");
        musiclist.add("http://13.233.184.187/Celebrity/music/Igcokama_Elisha_Uzozwa_Ngathi_Bamoza.mp3");
        musiclist.add("http://13.233.184.187/Celebrity/music/Ankhiyan_De_Nede _JordanSandhu.mp3");
        musiclist.add("http://13.233.184.187/Celebrity/music/Igcokama_Elisha_Uzozwa_Ngathi_Bamoza.mp3");
        musiclist.add("http://13.233.184.187/Celebrity/music/Ankhiyan_De_Nede _JordanSandhu.mp3");
        musiclist.add("http://13.233.184.187/Celebrity/music/Igcokama_Elisha_Uzozwa_Ngathi_Bamoza.mp3");*/
        setData();
    }

    private void setData() {
        artistamelist=MusicPlayer.artistlist;
        musiclist=MusicPlayer.songslist;
        musicnamelist=MusicPlayer.songnamelist;
        SharedPreferences preferences = getSharedPreferences(MusicPlayer.STORAGE, Context.MODE_PRIVATE);
        int audioIndex= preferences.getInt("audioIndex", 0);//return -1 if no data found
        playlist_adapter=new Playlist_Adapter(this,musiclist,musicnamelist,artistamelist, new Playlist_Adapter.OnItemClickListener(){
            @Override
            public void onItemClick(String item,int position) {
                Log.d("selected_musicitem","itm "+item+":"+position);
                Intent intentFilter=new Intent(MusicPlayer.Broadcast_Play_Playlist_Audio);
                intentFilter.putExtra("selectedmusic",position);
                sendBroadcast(intentFilter);

            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Playlist_Activity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        playlistrv.setLayoutManager(linearLayoutManager);
        playlistrv.setAdapter(playlist_adapter);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.back){
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {
        Intent i=new Intent(this, NetworkError.class);
        startActivity(i);
    }
}
