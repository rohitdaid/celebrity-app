package com.app.mthandenimanqele.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.app.mthandenimanqele.NetworkError;
import com.app.mthandenimanqele.NetworkStateReceiver;
import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.utils.Pref;
import com.app.mthandenimanqele.webservices.ApiService;
import com.app.mthandenimanqele.webservices.RetrofitClient;
import com.mikhaellopez.lazydatepicker.LazyDatePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfile extends AppCompatActivity implements View.OnClickListener, NetworkStateReceiver.NetworkStateReceiverListener {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.save)
    ImageView save;
    @BindView(R.id.et_fname)
    EditText et_fname;
    @BindView(R.id.et_lname)
    EditText et_lname;
    @BindView(R.id.et_dob)
    EditText et_dob;
    @BindView(R.id.et_mobile)
    EditText et_mobile;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_address)
    EditText et_address;
    @BindView(R.id.et_province)
    EditText et_province;
    @BindView(R.id.et_insta)
    EditText et_insta;
    @BindView(R.id.et_fb)
    EditText et_fb;
    @BindView(R.id.et_twitter)
    EditText et_twitter;
    @BindView(R.id.et_dob1)
    LazyDatePicker et_dob1;

    String fname,lname,email,mobile,dob,address,province,induna_name,branch,about_igcokama,
            preference,instagram,facebook,twitter,avatar;
    private int error;
    private ProgressDialog progressDialog;
    private String TAG=getClass().getName();
    private String birthdate;
    NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ButterKnife.bind(this);
        progressDialog=new ProgressDialog(this);
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        //information
        setData();
        back.setOnClickListener(this);
        save.setOnClickListener(this);
        et_dob.setOnClickListener(this);
        et_dob1.setOnDatePickListener(new LazyDatePicker.OnDatePickListener() {
            @Override
            public void onDatePick(Date dateSelected) {
                if(dateSelected!=null){
                    //String dt=et_dob1.getDate().getYear()+"-"+et_dob1.getDate().getMonth()+"-"+et_dob1.getDate().getDate();
                    int dayOfMonth=et_dob1.getDate().getDate();
                    int month=et_dob1.getDate().getMonth()+1;
                    int year=et_dob1.getDate().getYear()+1900;
                    String date = dayOfMonth + "/" + month + "/" + year;
                    String monthstring = null,dayofmonthString = null;
                    if (month < 10 && dayOfMonth < 10) {

                        monthstring = "0" + month;
                        dayofmonthString = "0" + dayOfMonth;
                        birthdate = year + "-" + monthstring + "-" + dayofmonthString + "T00:00:00.000Z";
                    } else if (month < 10 && dayOfMonth > 10) {

                        monthstring = "0" + month;
                        dayofmonthString=""+dayOfMonth;
                        birthdate = year + "-" + monthstring + "-" + dayOfMonth + "T00:00:00.000Z";
                    } else if (month > 10 && dayOfMonth < 10) {
                        dayofmonthString = "0" + dayOfMonth;
                        monthstring=""+month;
                        birthdate = year + "-" + month + "-" + dayofmonthString + "T00:00:00.000Z";
                    } else {
                        monthstring=""+month;
                        dayofmonthString=""+dayOfMonth;
                        birthdate = year + "-" + month + "-" + dayOfMonth + "T00:00:00.000Z";
                    }
                    String stdate = year+"-"+monthstring + "-" + dayofmonthString;
                    Log.d("date_selected ","datepick "+stdate);
                }else{
                    Log.d("date_selected ","date!pick "+et_dob1.getDate());
                }
            }
        });
        et_dob1.setOnDateSelectedListener(new LazyDatePicker.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Boolean dateSelected) {
                if(dateSelected){
                    Log.d("date_selected ","date "+et_dob1.getDate());
                }else{
                    Log.d("date_selected ","!date "+et_dob1.getDate());
                }
            }
        });

    }

    private void setData() {
        fname=Pref.getUserFirstname(getApplicationContext());
        lname=Pref.getUserLastname(getApplicationContext());
        email=Pref.getUserEmail(getApplicationContext());
        mobile=Pref.getUserMobile(getApplicationContext());
        dob=Pref.getUserDOB(getApplicationContext());
        avatar=Pref.getUSER_DP(getApplicationContext());
        //address
        address=Pref.getUSER_Address(getApplicationContext());
        province=Pref.getUSER_PROVINCE(getApplicationContext());
        branch=Pref.getUSER_BRANCH(getApplicationContext());
        //igockama elisha
        about_igcokama=Pref.getAbout_Igcokama(getApplicationContext());
        preference=Pref.getUSER_preference(getApplicationContext());
        induna_name=Pref.getInduna_Name(getApplicationContext());
        //social account links
        instagram=Pref.getUserInstagram(getApplicationContext());
        facebook=Pref.getUserFb(getApplicationContext());
        twitter=Pref.getUserTwitter(getApplicationContext());


        if(!instagram.equals("null")) {et_insta.setText(instagram);}
        if(!twitter.equals("null")) {et_twitter.setText(twitter);}
        if(!facebook.equals("null")) {et_fb.setText(facebook);}
        et_fname.setText(fname);
        et_lname.setText(lname);
        et_email.setText(email);
        et_mobile.setText(mobile);
        et_dob.setText(dob);
        Date date= null;
        try {
            //String date[]= TextUtils.StringSplitter.s
            date = new SimpleDateFormat("yyyy-MM-dd").parse(dob);
            //Log.d("setDate","date"+dob+"\n"+date);
            //et_dob1.setDateFormat(LazyDatePicker.DateFormat.MM_DD_YYYY);
            et_dob1.setDate(date);
            birthdate=dob;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        et_address.setText(address);
        et_province.setText(province);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.back){
            finish();
        }if(v.getId()==R.id.et_dob){
            dateofbirth();
        }
        if(v.getId()==R.id.save){
            fname=et_fname.getText().toString();
            lname=et_lname.getText().toString();
            mobile=et_mobile.getText().toString();
            email=et_email.getText().toString();
            dob=et_dob.getText().toString();
            address=et_address.getText().toString();
            province=et_province.getText().toString();
            facebook=et_fb.getText().toString();
            twitter=et_twitter.getText().toString();
            instagram=et_insta.getText().toString();
            EditText[] allFields={et_fname,et_lname,et_email,et_mobile,et_address,et_province};
            List<EditText> ErrorFields = new ArrayList<EditText>();
            for (EditText editText : allFields) {
                if (TextUtils.isEmpty(editText.getText())) {
                    ErrorFields.add(editText);
                }
            }

            if(ErrorFields.size()>0){
                EditText currentField = ErrorFields.get(0);
                currentField.setError("this field required");
                currentField.requestFocus();
                error=1;
            }
            else if(birthdate.length()==0){
                et_dob1.requestFocus();
                Toasty.warning(getApplicationContext(),"Enter date of birth",Toasty.LENGTH_SHORT).show();
            }
            else{
                updateProfile(fname,lname,mobile,email,address,induna_name,branch,province,birthdate,about_igcokama,preference,
                        facebook,twitter,instagram,avatar);
                Pref.putUserInstagram(getApplicationContext(),instagram);
                Pref.putUserFb(getApplicationContext(),facebook);
                Pref.putUserTwitter(getApplicationContext(),twitter);
            }
            /*Toasty.success(getApplicationContext(),"Changes saved",Toasty.LENGTH_SHORT).show();
            finish();*/
        }
    }

    private void dateofbirth() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(EditProfile.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = dayOfMonth + "/" + month + "/" + year;
                String monthstring = null,dayofmonthString = null;
                if (month < 10 && dayOfMonth < 10) {

                    monthstring = "0" + month;
                    dayofmonthString = "0" + dayOfMonth;
                    birthdate = year + "-" + monthstring + "-" + dayofmonthString + "T00:00:00.000Z";
                } else if (month < 10 && dayOfMonth > 10) {

                    monthstring = "0" + month;
                    dayofmonthString=""+dayOfMonth;
                    birthdate = year + "-" + monthstring + "-" + dayOfMonth + "T00:00:00.000Z";
                } else if (month > 10 && dayOfMonth < 10) {
                    dayofmonthString = "0" + dayOfMonth;
                    monthstring=""+month;
                    birthdate = year + "-" + month + "-" + dayofmonthString + "T00:00:00.000Z";
                } else {
                    monthstring=""+month;
                    dayofmonthString=""+dayOfMonth;
                    birthdate = year + "-" + month + "-" + dayOfMonth + "T00:00:00.000Z";
                }
                String stdate = year+"-"+monthstring + "-" + dayofmonthString;
                birthdate=stdate;
                et_dob.setText(stdate);
            }
        }, year, month, day);
        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        datePickerDialog.show();
    }
    private void updateProfile(String fname, String lname, String mobile, String email, String address, String induna_name, String branch, String province, String dob, String about_igcokama, String preference, String facebook, String twitter, String instagram, String avatar) {
        progressDialog.show();
        ApiService apiService= RetrofitClient.getClient().create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("first_name", fname);
            paramObject.put("last_name", lname);
            paramObject.put("mobile", mobile);
            paramObject.put("email", email);
            paramObject.put("address", address);
            paramObject.put("induna_name", induna_name);
            paramObject.put("branch", branch);
            paramObject.put("province", province);
            paramObject.put("dob", dob);
            paramObject.put("about_igcokama",about_igcokama );
            paramObject.put("preference", preference);
            paramObject.put("facebook_link", facebook);
            paramObject.put("twitter_link", twitter);
            paramObject.put("instagram_link", instagram);
            paramObject.put("avatar",avatar);
            Log.d("edit_profile","data "+paramObject);
            RequestBody body = RequestBody.create(MediaType.parse("text/plain"), String.valueOf((paramObject)));
            Call<ResponseBody> call=apiService.updateprofile(body,Pref.getUserToken(getApplicationContext()));
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful() && response.code()==200){
                        try {
                            JSONObject jsonObject=new JSONObject(response.body().string());
                            String message=jsonObject.getString("message");
                            Toasty.success(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                            finish();
                            //customType(ForgotPassActivity.this, "right-to-left");
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }else if(response.code()==422){
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            JSONObject errorObject = jsonObject.getJSONObject("errors");
                            String message = errorObject.getString("email");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                    else{
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            String message = jsonObject.getString("error");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d(TAG,"error "+t.getMessage());
                    Toasty.error(EditProfile.this, t.getLocalizedMessage(), Toasty.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {
        Intent i=new Intent(this, NetworkError.class);
        startActivity(i);
    }
}
