package com.app.mthandenimanqele.ui;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.mthandenimanqele.NetworkError;
import com.app.mthandenimanqele.NetworkStateReceiver;
import com.app.mthandenimanqele.adapter.SliderAdapter;
import com.app.mthandenimanqele.webservices.BaseUrl;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.app.mthandenimanqele.BuildConfig;
import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.adapter.Photos_RecycleView_Adapter;
import com.app.mthandenimanqele.adapter.Videos_Recycler_Adapter;
import com.app.mthandenimanqele.ui.musicplayer.MusicPlayer;
import com.app.mthandenimanqele.utils.Pref;
import com.app.mthandenimanqele.webservices.ApiService;
import com.app.mthandenimanqele.webservices.RetrofitClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BioActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener,View.OnClickListener {


    RecyclerView collection_recycleview;
    RecyclerView postlike_recycleview;
    ImageView instagram,facebook,twitter;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView navigationView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.viewall_photos)
    TextView viewall_photos;
    @BindView(R.id.viewall_videos)
    TextView viewall_videos;
    @BindView(R.id.website)
    TextView website;

    private ArrayList<String> photosCollection;
    private ArrayList<String> videosCollection;
    private Photos_RecycleView_Adapter photos_recycleView_adapter;
    private Videos_Recycler_Adapter videos_recycler_adapter;
    private CircularImageView bioimage;

    private Drawer result;
    private Bundle mSavedInstanceState;
    Resources res;
    private ViewPager mViewPager;
    private PostFragment adapter;
    private ProgressDialog progressDialog;
    public static List<String> imageslist;
    public static List<String> videoslist;
    NetworkStateReceiver networkStateReceiver;

    //slider
    ViewPager viewPager;
    TabLayout indicator;

    List<String> adslist;
    List<String> adslink;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio);
        ButterKnife.bind(this);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading data for you. Please wait!");
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        adapter = new PostFragment(getSupportFragmentManager());
        adapter.addFragment(new ImagesFragment(), "Images");
        adapter.addFragment(new VideosFragment(), "Videos");
        mViewPager.setAdapter(adapter);
        mViewPager.setCurrentItem(0);
        adapter.notifyDataSetChanged();
        instagram=findViewById(R.id.insta);
        facebook=findViewById(R.id.facebook);
        twitter=findViewById(R.id.twitter);
        instagram.setOnClickListener(this);
        facebook.setOnClickListener(this);
        twitter.setOnClickListener(this);
        viewall_photos.setOnClickListener(this);
        viewall_videos.setOnClickListener(this);
        website.setOnClickListener(this);
        res=getResources();
        mSavedInstanceState=savedInstanceState;
        collection_recycleview = findViewById(R.id.collection_recycleview);
        bioimage=findViewById(R.id.bioimage);
        postlike_recycleview = findViewById(R.id.postlike_recycleview);


        //slider
        viewPager=findViewById(R.id.viewPager);
        indicator=findViewById(R.id.indicator);

        //colorName.add("BLUE");


        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);

/*        getImagelist();
        getVideoslist();*/
        String[] videourls=res.getStringArray(R.array.videos);


        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withTranslucentStatusBar(true)
                .withHeaderBackground(R.drawable.logo_min)
                .withSavedInstance(mSavedInstanceState)
                .build();
        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .addDrawerItems(
                ).withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    if (drawerItem != null) {
                        if (drawerItem.getTag().toString().equals("LOG_OUT")) {
                            Pref.clearPref(getApplicationContext());
                            Toasty.success(getApplicationContext(),"Logout Successfully",Toasty.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                            finish();
                        } else if (drawerItem.getTag().toString().equals("Music")) {
                            Intent intent = new Intent(BioActivity.this, MusicPlayer.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else if (drawerItem.getTag().toString().equals("Profile")) {
                            Intent intent = new Intent(BioActivity.this, ProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else if (drawerItem.getTag().toString().equals("Share")) {
                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            /*sendIntent.putExtra(Intent.EXTRA_TEXT,
                                    "Hey check out IGCOKAMA ELISHA app at: https://play.google.com/store/apps/details?id=" + "com.ontrack.ontrackexpress");*/
                            sendIntent.putExtra(Intent.EXTRA_TEXT,
                                    "Hey check out *IGCOKAMA ELISHA* app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                            sendIntent.setType("text/plain");
                            startActivity(sendIntent);
                        }
                    }
                    return false;
                })
                .withSavedInstance(mSavedInstanceState)
                .withShowDrawerOnFirstLaunch(false)
                .build();
        result.addStickyFooterItem(new PrimaryDrawerItem().withName("Log Out").withIcon(R.drawable.ic_back).withTag("LOG_OUT"));
        PrimaryDrawerItem home = new PrimaryDrawerItem().withName("Home").withIcon(R.drawable.ic_home_black_24dp).withTag("Home");
        result.addItem(home);
        PrimaryDrawerItem profile = new PrimaryDrawerItem().withName("Profile").withIcon(R.drawable.ic_account_circle_black_24dp).withTag("Profile").withSelectable(true);
        result.addItem(profile);
        PrimaryDrawerItem music = new PrimaryDrawerItem().withName("Music").withIcon(R.drawable.ic_library_music_black_24dp).withTag("Music");
        result.addItem(music);
        PrimaryDrawerItem share = new PrimaryDrawerItem().withName("Share").withIcon(R.drawable.ic_share_black_24dp).withTag("Share").withSelectable(false);
        result.addItem(share);
        result.addItem(new DividerDrawerItem());
        result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
        result.getActionBarDrawerToggle().getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
    }

    private void getVideoslist() {
        progressDialog.show();
        ApiService apiService= RetrofitClient.getClient().create(ApiService.class);
        Call<ResponseBody> call=apiService.videoslist(Pref.getUserToken(getApplicationContext()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful() && response.code()==200){
                    try {
                        JSONObject jsonObject=new JSONObject(response.body().string());
                        JSONArray dataArray=jsonObject.getJSONArray("data");
                        int arraysize=dataArray.length();
                        videosCollection = new ArrayList<>();
                        if(arraysize!=0){
                            videoslist=new ArrayList<>();
                            for(int i=0;i<arraysize;i++){
                                JSONObject imagedata= (JSONObject) dataArray.get(i);
                                String path=imagedata.getString("path");
                                videoslist.add(path);
                            }

                            if(videoslist.size()>5) {
                                for (int i = 0; i < 5; i++) {
                                    videosCollection.add(i, videoslist.get(i));
                                }
                            }else{
                                videosCollection.addAll(videoslist);
                            }
                            //PostLike  Adapter
                            videos_recycler_adapter = new Videos_Recycler_Adapter(getApplicationContext(),videosCollection);
                            LinearLayoutManager layoutManager2 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                            postlike_recycleview.setLayoutManager(layoutManager2);
                            postlike_recycleview.setAdapter(videos_recycler_adapter);
                        }


                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }else if(response.code()==401){
                    try {
                        JSONObject jsonObject=new JSONObject(response.errorBody().string());
                        String msg= jsonObject.getString("message");
                        Toasty.warning(getApplicationContext(),msg,Toasty.LENGTH_SHORT).show();
                        Pref.clearPref(getApplicationContext());
                        progressDialog.dismiss();
                        startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void getImagelist() {
        progressDialog.show();
        ApiService apiService= RetrofitClient.getClient().create(ApiService.class);
        Call<ResponseBody> call=apiService.imageslist(Pref.getUserToken(getApplicationContext()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful() && response.code()==200){
                    try {
                        JSONObject jsonObject=new JSONObject(response.body().string());
                        JSONArray dataArray=jsonObject.getJSONArray("data");
                        int arraysize=dataArray.length();
                        photosCollection = new ArrayList<>();
                        if(arraysize!=0){
                            imageslist=new ArrayList<>();
                            for(int i=0;i<arraysize;i++){
                                JSONObject imagedata= (JSONObject) dataArray.get(i);
                                String path=imagedata.getString("path");
                                imageslist.add(path);
                            }

                            if(imageslist.size()>5) {
                                for (int i = 0; i < 5; i++) {
                                    photosCollection.add(i, imageslist.get(i));
                                }
                            }else{
                                photosCollection.addAll(imageslist);

                            }

                            photos_recycleView_adapter = new Photos_RecycleView_Adapter(BioActivity.this,photosCollection);
                            LinearLayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                            collection_recycleview.setLayoutManager(layoutManager1);
                            collection_recycleview.setAdapter(photos_recycleView_adapter);
                        }


                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }else if(response.code()==401){
                    try {
                        JSONObject jsonObject=new JSONObject(response.errorBody().string());
                        String msg= jsonObject.getString("message");
                        Toasty.warning(getApplicationContext(),msg,Toasty.LENGTH_SHORT).show();
                        Pref.clearPref(getApplicationContext());
                        progressDialog.dismiss();
                        startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.insta){
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.insta))));
        }if(v.getId()==R.id.facebook){
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.fb))));
        }if(v.getId()==R.id.twitter){
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.twitter))));
        }if(v.getId()==R.id.viewall_photos){
            startActivity(new Intent(getApplicationContext(),New_BioActivity.class)
                    .putExtra("value",0));

        }if(v.getId()==R.id.viewall_videos){
            startActivity(new Intent(getApplicationContext(),New_BioActivity.class)
                    .putExtra("value",1));
        }
        if(v.getId()==R.id.website){
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.website))));
        }
    }
    @Override
    public void onBackPressed() {
        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getImagelist();
        getVideoslist();
        getAdvertisementList();
    }

    private void getAdvertisementList() {
        progressDialog.show();
        ApiService apiService= RetrofitClient.getClient().create(ApiService.class);
        Call<ResponseBody> call=apiService.advertisement(Pref.getUserToken(getApplicationContext()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful() && response.code()==200){
                    try {
                        JSONObject jsonObject=new JSONObject(response.body().string());
                        JSONArray dataArray=jsonObject.getJSONArray("data");
                        int arraysize=dataArray.length();
                        adslist=new ArrayList<>();
                        adslink=new ArrayList<>();
                        if(arraysize!=0){
                            for(int i=0;i<arraysize;i++){
                                JSONObject addData= (JSONObject) dataArray.get(i);
                                String path=addData.getString("path");
                                String link=addData.getString("link");
                                adslist.add(path);
                                adslink.add(link);
                            }

                            //AdsAdapter
                            viewPager.setAdapter(new SliderAdapter(getApplicationContext(), adslist,adslink));
                            indicator.setupWithViewPager(viewPager, true);
                        }


                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }else if(response.code()==401){
                    try {
                        JSONObject jsonObject=new JSONObject(response.errorBody().string());
                        String msg= jsonObject.getString("message");
                        Toasty.warning(getApplicationContext(),msg,Toasty.LENGTH_SHORT).show();
                        Pref.clearPref(getApplicationContext());
                        progressDialog.dismiss();
                        startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {
        Intent i=new Intent(this, NetworkError.class);
        startActivity(i);
    }

    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            BioActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (adslink.size() > 0) {
                        if (viewPager.getCurrentItem() < adslink.size() - 1) {
                            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                        } else {
                            viewPager.setCurrentItem(0);
                        }
                    }
                }
            });
        }
    }
}