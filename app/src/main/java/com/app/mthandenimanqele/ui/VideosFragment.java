package com.app.mthandenimanqele.ui;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.adapter.Videos_Recycler_Adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VideosFragment extends Fragment {
    Videos_Recycler_Adapter videos_recycler_adapter;
    List<String> videoslist=new ArrayList<>();;
    Resources res;
    RecyclerView recycler_view;
    public VideosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        res=getResources();
        View view=inflater.inflate(R.layout.fragment_videos,container,false);
        recycler_view=view.findViewById(R.id.recycler_view);
        String[] videos=res.getStringArray(R.array.videos);
        videoslist.addAll(Arrays.asList(videos));
        Log.d("imagesfragment ","videoslist "+videoslist.toString());
        videos_recycler_adapter=new Videos_Recycler_Adapter(getContext(),videoslist);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recycler_view.setLayoutManager(layoutManager1);
        recycler_view.setAdapter(videos_recycler_adapter);
        return view;
    }
}
