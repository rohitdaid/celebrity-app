package com.app.mthandenimanqele.ui.musicplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.mthandenimanqele.NetworkError;
import com.app.mthandenimanqele.NetworkStateReceiver;
import com.app.mthandenimanqele.ui.LoginActivity;
import com.app.mthandenimanqele.utils.Pref;
import com.app.mthandenimanqele.webservices.ApiService;
import com.app.mthandenimanqele.webservices.RetrofitClient;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.service.MediaPlayerService;
import com.app.mthandenimanqele.webservices.BaseUrl;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import me.tankery.lib.circularseekbar.CircularSeekBar;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MusicPlayer extends AppCompatActivity implements View.OnClickListener, NetworkStateReceiver.NetworkStateReceiverListener {

    public static CircularSeekBar seekbar;

    public static TextView currentMusictime;

    @BindView(R.id.playprevious)
    ImageView previous;
    @BindView(R.id.playnext)
    ImageView next;
    @BindView(R.id.back)
    ImageView back;
    NetworkStateReceiver networkStateReceiver;

    public static TextView musictime;
    public  static ImageView play;
    public  static ImageView pause;
    @BindView(R.id.menu)
    ImageView playlisticon;
    public static TextView musicname,musicdesc;
    public static int duration;
    public  static MediaPlayer mediaPlayer;
    private int pauseat=0;
    public static int dividetym;
    public static List<String> songslist,artistlist,songnamelist;
    private String song;
    public static int loadAudioIndex=0;
    private ProgressDialog progressDialog;
    //service
    boolean serviceBound = false;
    public static final String Broadcast_PLAY_NEW_AUDIO = "com.ontrack.mthandenimanqele.PlayNewAudio";
    public static final String Broadcast_SeekBar_Update = "com.ontrack.mthandenimanqele.SeekBar_Update";
    public static final String Broadcast_RESUME_AUDIO = "com.ontrack.mthandenimanqele.Resume_Audio";
    public static final String Broadcast_PAUSE_AUDIO = "com.ontrack.mthandenimanqele.Pause_Audio";
    public static String Broadcast_Play_Playlist_Audio="com.ontrack.mthandenimanqele.Playlist_Audio";
    private MediaPlayerService player;
    public static String STORAGE = "audioplayer.STORAGE";
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;
    String updateseekBar_receivedValue;
    String playedmusictime;
    public static int mpcurrentposition;
    private int audioIndex;
    private String songnm="",songduration=null,artistnm=null;
    private BroadcastReceiver updateseekBar = new BroadcastReceiver() {
        /** Receives the broadcast that has been fired */
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction()==Broadcast_SeekBar_Update){
                //HERE YOU WILL GET VALUES FROM BROADCAST THROUGH INTENT EDIT YOUR TEXTVIEW///////////
                updateseekBar_receivedValue=intent.getStringExtra("seekvalue");
                String musictym=intent.getStringExtra("musictime");
                //Log.d("getmusictym","tym "+musictym);
                musictime.setText(musictym);
                editor.putString("songduration",musictym);
                /*playedmusictime=intent.getStringExtra("musictime");*/
                /*mpcurrentposition=intent.getStringExtra("currentposition");*/
                seekBarHandler(duration);
                pauseat= 1;
                currentMusictime.setText(updateseekBar_receivedValue);
            }
        }
    };
    private BroadcastReceiver playmusicviaplaylist = new BroadcastReceiver() {
        /** Receives the broadcast that has been fired */
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction()==Broadcast_Play_Playlist_Audio){
                //HERE YOU WILL GET VALUES FROM BROADCAST THROUGH INTENT EDIT YOUR TEXTVIEW///////////
                int musicposition=intent.getIntExtra("selectedmusic",0);
                audioIndex=musicposition;
                Log.d("getmusictym","musicposition "+musicposition);
                playAudio(audioIndex);
                songnm=songnamelist.get(audioIndex);
                musicname.setText(songnm);
                artistnm=artistlist.get(audioIndex);
                musicdesc.setText(artistnm);
                editor.putString("songname",songnm);
                editor.putString("artistname",artistnm);
                editor.apply();
            }
        }
    };



    private void seekBarHandler(int playedmusictime) {
        long musicactualduration=TimeUnit.MILLISECONDS.toSeconds(playedmusictime);
        long mins = musicactualduration / 60;
        musicactualduration = musicactualduration - mins * 60;
        long secs = musicactualduration;
        musictime.setText(""+mins+":"+secs);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_player);
        ButterKnife.bind(this);
        context=this;
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        mediaPlayer =new MediaPlayer();
        play=findViewById(R.id.play);
        pause=findViewById(R.id.pause);
        seekbar=findViewById(R.id.seekbar);
        musicname=findViewById(R.id.musicname);
        musicdesc=findViewById(R.id.musicdesc);
        musictime=findViewById(R.id.musictime);
        currentMusictime=findViewById(R.id.current_musictime);
        back.setOnClickListener(this);
        play.setOnClickListener(this);
        pause.setOnClickListener(this);
        previous.setOnClickListener(this);
        next.setOnClickListener(this);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Please wait");
        //loadmusicplayerlist
        setData();
        playlisticon.setOnClickListener(this);
        seekbar.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, float progress, boolean fromUser) {

            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {
                float touchposition=seekBar.getProgress();
                int time=(int) touchposition*dividetym;
                try {
                    if (MediaPlayerService.mediaPlayer != null && MediaPlayerService.mediaPlayer.isPlaying()) {
                        MediaPlayerService.resumePosition = time;
                        MediaPlayerService.mediaPlayer.seekTo(time);
                        MediaPlayerService.mediaPlayer.start();
                        Log.d("music_player", "progress value" + touchposition + "*" + dividetym + "=" + time);
                    } else {
                        MediaPlayerService.resumePosition = time;
                    }
                }catch(IllegalStateException e){
                    Log.d("Music_Player",""+e.getMessage());
                }
                //new MediaPlayerService.SeekBarHandler().execute();

            }
        });
    }

    private void setData() {
        getsongslist();
        //registerReceiver();
        register_SeekValueUpdater();
        register_PlayMusicViaPlaylist();
        Resources res=getResources();
        preferences = getSharedPreferences(MusicPlayer.STORAGE, Context.MODE_PRIVATE);
        editor=preferences.edit();
        audioIndex= preferences.getInt("audioIndex", 0);//return -1 if no data found
        songnm=preferences.getString("songname",null);
        artistnm=preferences.getString("artistname",null);
        songduration=preferences.getString("songduration",null);
        /*songslist=new ArrayList<>();
        songnamelist=new ArrayList<>();
        artistlist=new ArrayList<>();*/
        /*String[] songs=res.getStringArray(R.array.songs);
        String[] songsname=res.getStringArray(R.array.songsname);
        String[] artistname=res.getStringArray(R.array.artistname);
        for (int i=0;i<songs.length;i++){
            songslist.add(i,BaseUrl.musicurl+songs[i]);
        }
        songnamelist.addAll(Arrays.asList(songsname));
        artistlist.addAll(Arrays.asList(artistname));*/
        if(songnm != null ){
            musicname.setText(songnm);
            musicdesc.setText(artistnm);
            musictime.setText(songduration);
        }else{
            if(songnamelist!=null &&  songnamelist.size()!=0){
                musicname.setText(songnamelist.get(0));
                musictime.setText("4:50");
                musicdesc.setText(artistlist.get(0));
            }
        }
        if(serviceBound){
            if(MediaPlayerService.mediaPlayer.isPlaying()){
                play.setVisibility(View.GONE);
                pause.setVisibility(View.VISIBLE);
            }
        }
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    private void getsongslist() {
        progressDialog.show();
        ApiService apiService= RetrofitClient.getClient().create(ApiService.class);
        Call<ResponseBody> call=apiService.songslist(Pref.getUserToken(getApplicationContext()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful() && response.code()==200){
                    try {
                        JSONObject jsonObject=new JSONObject(response.body().string());
                        JSONArray dataArray=jsonObject.getJSONArray("data");
                        int arraysize=dataArray.length();
                        if(arraysize!=0){
                            songslist=new ArrayList<>();
                            songnamelist=new ArrayList<>();
                            artistlist=new ArrayList<>();
                            for(int i=0;i<arraysize;i++){
                                JSONObject imagedata= (JSONObject) dataArray.get(i);
                                String path=imagedata.getString("path");
                                String songtitle=imagedata.getString("title");
                                //JSONObject singerObject=imagedata.getJSONObject("singer");
                                songslist.add(i,path);
                                songnamelist.add(songtitle);
                                JSONArray singerarray=imagedata.getJSONArray("singer");
                                int size=singerarray.length();

                                for (int j=0;j<size;j++){
                                    JSONObject singerObject= (JSONObject) singerarray.get(j);
                                    //Log.d("singername_list","position "+i+singerObject);
                                    JSONObject artistObject=singerObject.getJSONObject("artist");
                                    String fname=artistObject.getString("first_name");
                                    String lname=artistObject.getString("last_name");
                                    String artistnm="";
                                    String artistname=fname+" "+lname;
                                    if(j==0){
                                        artistlist.add(i,artistnm+artistname);
                                    }else{
                                        artistnm=artistlist.get(i);
                                        artistlist.add(i,artistnm+","+artistname);
                                    }


                                    //Log.d("singername_list","position "+i+fname+lname);
                                }
                                Log.d("singername_list","position "+i+artistlist.get(i));
                            }
                        }
                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if(response.code()==401){
                    try {
                        JSONObject jsonObject=new JSONObject(response.errorBody().string());
                        String msg= jsonObject.getString("message");
                        Toasty.warning(getApplicationContext(),msg,Toasty.LENGTH_SHORT).show();
                        Pref.clearPref(getApplicationContext());
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.play){
            if(pauseat==0){
                //playmusic();
                playAudio(audioIndex);
                //new SeekBarHandler().execute();
            }else{
                /*mediaPlayer.seekTo(pauseat);
                mediaPlayer.start();
                new SeekBarHandler().execute();*/
                Intent intent=new Intent(Broadcast_RESUME_AUDIO);
                intent.putExtra("resumeeat",updateseekBar_receivedValue);
                sendBroadcast(intent);
            }
            pause.setVisibility(View.VISIBLE);
            play.setVisibility(View.GONE);
        }if(v.getId()==R.id.pause){
            Intent intent=new Intent(Broadcast_PAUSE_AUDIO);
            intent.putExtra("pauseat",updateseekBar_receivedValue);
            sendBroadcast(intent);
            /*pauseat=mediaPlayer.getCurrentPosition();
            mediaPlayer.pause();*/
            pause.setVisibility(View.GONE);
            play.setVisibility(View.VISIBLE);
        }
        if(v.getId()==R.id.menu){
            startActivity(new Intent(getApplicationContext(),Playlist_Activity.class));
        }
        if(v.getId()==R.id.back){
            /*startActivity(new Intent(getApplicationContext(),Playlist_Activity.class));*/
            finish();
        }
        if(v.getId()==R.id.playprevious){
            if(audioIndex>0){
                audioIndex=audioIndex-1;
            }else{
                audioIndex=songslist.size()-1;
            }
            playAudio(audioIndex);
        }
        if(v.getId()==R.id.playnext){
            if(audioIndex<songslist.size()){
                audioIndex=audioIndex+1;
            }else{
                audioIndex=0;
            }
            playAudio(audioIndex);
        }
    }


    //Binding this Client to the AudioPlayer Service
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MediaPlayerService.LocalBinder binder = (MediaPlayerService.LocalBinder) service;
            player = binder.getService();
            serviceBound = true;

            //Toast.makeText(MusicPlayer.this, "Service Bound", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
            //   Toast.makeText(MusicPlayer.this, "Service not Bound", Toast.LENGTH_SHORT).show();
        }
    };
    public List<String> loadAudio() {
        Gson gson = new Gson();
        String json = preferences.getString("audioArrayList", null);
        Log.d("notification_service","loadAudio "+json);
        Type type = new TypeToken<List<String>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void storeAudio(List<String> arrayList) {
        Gson gson = new Gson();
        String json = gson.toJson(arrayList);
        editor.putString("audioArrayList", json);
        Log.d("notification_service","storeAudio "+json);
        editor.apply();
    }
    public void storeAudioIndex(int index) {
        Log.d("indexsize ",""+index);
        if(index<songslist.size()){
            editor.putInt("audioIndex", index);
            editor.putString("songname",songnamelist.get(index));
            editor.putString("artistname",artistlist.get(index));
            editor.apply();
        }
    }

    public int loadAudioIndex() {
        return preferences.getInt("audioIndex", -1);//return -1 if no data found
    }
    public void clearCachedAudioPlaylist() {
        editor.clear();
        editor.commit();
    }
    private void playAudio(int index) {
        //Check is service is active
        if (!serviceBound) {
            storeAudio(songslist);
            storeAudioIndex(index);
            Intent playerIntent = new Intent(this, MediaPlayerService.class);
            /*playerIntent.putExtra("media", media);*/
            startService(playerIntent);
            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
            Log.d("chkaudio","if index "+index+":"+audioIndex);
        } else {
            storeAudioIndex(index);
            //Service is active
            //Send media with BroadcastReceiver
            Intent broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);
            sendBroadcast(broadcastIntent);
            Log.d("chkaudio","else index "+index+":"+audioIndex);
        }
    }

    private void register_SeekValueUpdater() {
        //Register playNewMedia receiver
        IntentFilter filter = new IntentFilter(MusicPlayer.Broadcast_SeekBar_Update);
        registerReceiver(updateseekBar, filter);
    }
    private void register_PlayMusicViaPlaylist() {
        //Register playNewMedia receiver
        IntentFilter filter = new IntentFilter(MusicPlayer.Broadcast_Play_Playlist_Audio);
        registerReceiver(playmusicviaplaylist, filter);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean("ServiceState", serviceBound);
        super.onSaveInstanceState(savedInstanceState);
    }
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        serviceBound = savedInstanceState.getBoolean("ServiceState");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(MediaPlayerService.mediaPlayer!=null){
            try{
                if(MediaPlayerService.mediaPlayer.isPlaying()){
                    play.setVisibility(View.GONE);
                    pause.setVisibility(View.VISIBLE);
                }
            }catch(IllegalStateException e){

            }
        }
        setData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (serviceBound) {
            // unbindService(serviceConnection);
            unregisterReceiver(updateseekBar);
            unregisterReceiver(playmusicviaplaylist);
            //service is active
            // player.stopSelf();
        }
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {

        if(MediaPlayerService.mediaPlayer!=null && MediaPlayerService.mediaPlayer.isPlaying()){
            /*int position= MediaPlayerService.mediaPlayer.getCurrentPosition();
            MediaPlayerService.resumePosition=position;
            MediaPlayerService.mediaPlayer.pause();
            play.setVisibility(View.VISIBLE);
            pause.setVisibility(View.GONE);*/
            Intent intent=new Intent(Broadcast_PAUSE_AUDIO);
            intent.putExtra("pauseat",updateseekBar_receivedValue);
            sendBroadcast(intent);
            pause.setVisibility(View.GONE);
            play.setVisibility(View.VISIBLE);

        }
        Intent i=new Intent(this, NetworkError.class);
        startActivity(i);
    }

   /* @Override
    protected void onStop() {
        super.onStop();
        if (serviceBound) {
            unbindService(serviceConnection);
            //unregisterReceiver(updateseekBar);
            //unregisterReceiver(playmusicviaplaylist);
            //service is active
            //player.stopSelf();
        }
    }*/
}