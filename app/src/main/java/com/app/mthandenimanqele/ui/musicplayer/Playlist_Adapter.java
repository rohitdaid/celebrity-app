package com.app.mthandenimanqele.ui.musicplayer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.webservices.BaseUrl;

import java.util.List;

public class Playlist_Adapter extends RecyclerView.Adapter {
    List<String> musiclist;
    List<String> musicnmlist;
    List<String> artistnmlist;
    Context mcontext;
    private final OnItemClickListener listener;
    public String musicurl;
    public Playlist_Adapter(Context context, List<String> musiclist,List<String> musicnamelist,List<String> artistnamelist,OnItemClickListener listner) {
        this.musiclist=musiclist;
        this.mcontext=context;
        this.listener=listner;
        this.musicnmlist=musicnamelist;
        this.artistnmlist=artistnamelist;
    }
    public interface OnItemClickListener {
        void onItemClick(String item,int itemposition);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.playlist_items, parent, false);

        return new RowViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        RowViewHolder rowViewHolder = (RowViewHolder) holder;
        musicurl= BaseUrl.musicurl;
        rowViewHolder.songname.setText(musicnmlist.get(position)+"-"+artistnmlist.get(position));
        rowViewHolder.albumname.setText("Is'korokoro sami");
        rowViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(musicurl+musiclist.get(position),position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return musiclist.size();
    }
    public class RowViewHolder extends RecyclerView.ViewHolder {
        protected TextView songname,albumname;
        protected ImageView albumart;

        public RowViewHolder(View itemView) {
            super(itemView);
            songname = itemView.findViewById(R.id.songname);
            albumname = itemView.findViewById(R.id.albumname);
            albumart = itemView.findViewById(R.id.album_art);

        }
    }
}
