package com.app.mthandenimanqele.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.utils.Pref;
import com.app.mthandenimanqele.webservices.ApiService;
import com.app.mthandenimanqele.webservices.RetrofitClient;
import com.mikhaellopez.lazydatepicker.LazyDatePicker;
import com.mikhaellopez.lazydatepicker.LazyLocalDatePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {


    @BindView(R.id.tv_register)
    TextView tv_register;

    @BindView(R.id.tv_register_msg)
    TextView tv_register_msg;

    @BindView(R.id.termandcondition)
    TextView termandcondition;

    @BindView(R.id.tv_to_terms)
    TextView tv_to_terms;

    @BindView(R.id.already_hv_account)
    TextView already_hv_account;

    @BindView(R.id.tv_airtime)
    TextView tv_airtime;

    @BindView(R.id.tvfavsong)
    TextView tvfavsong;

    @BindView(R.id.tvPrefer)
    TextView tvPrefer;

    @BindView(R.id.tvSim)
    TextView tvSim;


    @BindView(R.id.eng_translate)
    ImageView eng_translate;

    @BindView(R.id.zulu_translate)
    ImageView zulu_translate;



    @BindView(R.id.btn_register)
    Button register;

    @BindView(R.id.tv_to_login)
    TextView login;

    @BindView(R.id.tvorderSim)
    TextView tvorderSim;

    @BindView(R.id.et_favsong)
    EditText et_favsong;

    @BindView(R.id.tvrefer)
    TextView tvrefer;
    @BindView(R.id.rg_ordersim)
    RadioGroup rg_orderSim;

    @BindView(R.id.et_dob1)
    LazyDatePicker et_dob1;

    @BindView(R.id.et_dob)
    EditText dob;
    @BindView(R.id.et_first_name)
    EditText et_first_name;
    @BindView(R.id.et_last_name)
    EditText et_last_name;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_mobileno)
    EditText et_mobileno;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.et_confirm_password)
    EditText et_confirm_password;
    @BindView(R.id.et_address)
    EditText et_address;
    @BindView(R.id.et_induna_name)
    EditText et_induna_name;
    @BindView(R.id.et_branch_name)
    EditText et_branch_name;
    @BindView(R.id.et_province)
    EditText et_province;
    @BindView(R.id.et_like)
    EditText et_like;
    @BindView(R.id.rg_induna)
    RadioGroup rg_induna;
    @BindView(R.id.rb_indunayes)
    RadioButton rb_indunayes;
    @BindView(R.id.rb_indunano)
    RadioButton rb_indunano;
    @BindView(R.id.rg_sim)
    RadioGroup rg_sim;
    @BindView(R.id.rb_yes)
    RadioButton sim_yes;
    @BindView(R.id.rb_no)
    RadioButton sim_no;
    @BindView(R.id.rg_prefer)
    RadioGroup rg_prefer;
    @BindView(R.id.rb_preferyes)
    RadioButton prefer_yes;
    @BindView(R.id.rb_preferno)
    RadioButton prefer_no;
    @BindView(R.id.rg_refer)
    RadioGroup rg_refer;
    @BindView(R.id.refer_yes)
    RadioButton refer_yes;
    @BindView(R.id.refer_no)
    RadioButton refer_no;
    @BindView(R.id.mthandenisimcard)
    EditText et_mthandenisimcard;
    @BindView(R.id.et_refer_personnm)
    EditText et_refer_personnm;
    @BindView(R.id.et_refer_personno)
    EditText et_refer_personno;
    private String birthdate;
    private String fname,lname,address,province,prefer,induname,txtdob,email,password,confrmpaswrd,branchnm,like,rg,mobile;
    private String simcard, refer,mthandenisimcard,referperson_name,referperson_mob,order_new,fav_song;
    ProgressDialog progressDialog;
    private String TAG=getClass().getName();
    private int error;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ButterKnife.bind(this);
        progressDialog=new ProgressDialog(this);
        login.setOnClickListener(this);
        register.setOnClickListener(this);
        dob.setOnClickListener(this);
        eng_translate.setOnClickListener(this);
        zulu_translate.setOnClickListener(this);
        et_dob1.setOnDatePickListener(new LazyDatePicker.OnDatePickListener() {
            @Override
            public void onDatePick(Date dateSelected) {
                if(dateSelected!=null){
                    //String dt=et_dob1.getDate().getYear()+"-"+et_dob1.getDate().getMonth()+"-"+et_dob1.getDate().getDate();
                    int dayOfMonth=et_dob1.getDate().getDate();
                    int month=et_dob1.getDate().getMonth()+1;
                    int year=et_dob1.getDate().getYear()+1900;
                    String date = dayOfMonth + "/" + month + "/" + year;
                    String monthstring = null,dayofmonthString = null;
                    if (month < 10 && dayOfMonth < 10) {

                        monthstring = "0" + month;
                        dayofmonthString = "0" + dayOfMonth;
                        birthdate = year + "-" + monthstring + "-" + dayofmonthString + "T00:00:00.000Z";
                    } else if (month < 10 && dayOfMonth > 10) {

                        monthstring = "0" + month;
                        dayofmonthString=""+dayOfMonth;
                        birthdate = year + "-" + monthstring + "-" + dayOfMonth + "T00:00:00.000Z";
                    } else if (month > 10 && dayOfMonth < 10) {
                        dayofmonthString = "0" + dayOfMonth;
                        monthstring=""+month;
                        birthdate = year + "-" + month + "-" + dayofmonthString + "T00:00:00.000Z";
                    } else {
                        monthstring=""+month;
                        dayofmonthString=""+dayOfMonth;
                        birthdate = year + "-" + month + "-" + dayOfMonth + "T00:00:00.000Z";
                    }
                    String stdate = year+"-"+monthstring + "-" + dayofmonthString;
                    Log.d("date_selected ","datepick "+stdate);
                }else{
                    Log.d("date_selected ","date!pick "+et_dob1.getDate());
                }
            }
        });
        et_dob1.setOnDateSelectedListener(new LazyDatePicker.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Boolean dateSelected) {
                if(dateSelected){
                    Log.d("date_selected ","date "+et_dob1.getDate());
                }else{
                    Log.d("date_selected ","!date "+et_dob1.getDate());
                }
            }
        });
        rg_prefer.setOnCheckedChangeListener(this);
        rg_sim.setOnCheckedChangeListener(this);
        rg_refer.setOnCheckedChangeListener(this);
        rg_orderSim.setOnCheckedChangeListener(this);
        rg_induna.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.tv_to_login){
            startActivity(new Intent(this,LoginActivity.class));
        }
        if(v.getId()==R.id.et_dob){
            dateofbirth();
        }
        /*if(v.getId()==R.id.eng_translate){
            //translate_to_zulu();
        }
        if(v.getId()==R.id.zulu_translate){
            //translate_to_engish();
        }*/
        if(v.getId()==R.id.btn_register){
            checkEmptyFields();
            /*fname=et_first_name.getText().toString();
            lname=et_last_name.getText().toString();
            email=et_email.getText().toString();
            password=et_password.getText().toString();
            confrmpaswrd=et_confirm_password.getText().toString();
            txtdob=dob.getText().toString();
            address=et_address.getText().toString();
            induname=et_induna_name.getText().toString();
            branchnm=et_branch_name.getText().toString();
            province=et_province.getText().toString();
            like=et_like.getText().toString();
            String devicetype="Android";
            String devicetoken="25134263u1gbhr";
            rg="Yes";

            if(fname.isEmpty()||lname.isEmpty()||email.isEmpty()||password.isEmpty()||confrmpaswrd.isEmpty()||
                    txtdob.isEmpty()||address.isEmpty()||induname.isEmpty()||branchnm.isEmpty()||
                    province.isEmpty()||like.isEmpty()*//*||prefer.isEmpty() ||simcard.isEmpty()||refer.isEmpty()*//*){
                Toasty.warning(getApplicationContext(),"Enter all reuired fields",Toasty.LENGTH_SHORT).show();
            } else if(rg_prefer.getCheckedRadioButtonId()==-1){
                RadioButton radioButton=findViewById(R.id.rb_preferno);
                radioButton.setError("Select any one");
            }
            else if(rg_sim.getCheckedRadioButtonId()==-1){
                RadioButton radioButton=findViewById(R.id.rb_no);
                radioButton.setError("Select any one");
            }else if(rg_refer.getCheckedRadioButtonId()==-1){
                RadioButton radioButton=findViewById(R.id.refer_no);
                radioButton.setError("Select any one");
            }else if(simcard.equalsIgnoreCase("YES")){
                mthandenisimcard=et_mthandenisimcard.getText().toString();
                if(mthandenisimcard.isEmpty()){
                    et_mthandenisimcard.setError("Enter sim card no.");
                }
            }else if(refer.equalsIgnoreCase("YES")){
                referperson_name=et_refer_personnm.getText().toString();
                referperson_mob=et_refer_personno.getText().toString();
                if(referperson_name.isEmpty()){
                    et_refer_personnm.setError("Enter person name");
                }else if(referperson_mob.isEmpty()){
                    et_refer_personno.setError("Enter person contact no.");
                }
            }
            else if(!password.equals(confrmpaswrd)){
                Toasty.warning(getApplicationContext(),"Password Doesn't Match",Toasty.LENGTH_SHORT).show();
            }
            else{
                register(fname,lname,email,address,password,induname,branchnm,province,txtdob,devicetype,devicetoken
                ,like,prefer,simcard);
            }*/
        }
    }

    private void checkEmptyFields() {
        error=0;
        //progressDialog.show();
        fname=et_first_name.getText().toString();
        lname=et_last_name.getText().toString();
        mobile=et_mobileno.getText().toString();
        email=et_email.getText().toString();
        password=et_password.getText().toString();
        confrmpaswrd=et_confirm_password.getText().toString();
        txtdob=dob.getText().toString();
        address=et_address.getText().toString();
        branchnm=et_branch_name.getText().toString();
        province=et_province.getText().toString();
        like=et_like.getText().toString();
        fav_song=et_favsong.getText().toString();
        String devicetype="Android";
        String devicetoken="25134263u1gbhr";
        EditText[] allFields={et_first_name,et_last_name,et_email,et_mobileno,et_password,et_confirm_password
        ,et_address,et_province,et_like,et_favsong};
        List<EditText> ErrorFields = new ArrayList<EditText>();
        for (EditText editText : allFields) {
            if (TextUtils.isEmpty(editText.getText())) {
                ErrorFields.add(editText);
            }
        }
        if(ErrorFields.size()>0){
            EditText currentField = ErrorFields.get(0);
            currentField.setError("this field required");
            currentField.requestFocus();
            error=1;
        }else if(birthdate==null){
            et_dob1.requestFocus();
            Toasty.warning(getApplicationContext(),"Enter date of birth",Toasty.LENGTH_SHORT).show();
        }else if(!rb_indunayes.isChecked() && !rb_indunano.isChecked()){
            rb_indunano.setError("Select any one");
            error=1;
        }else if(rb_indunayes.isChecked() || rb_indunano.isChecked()){
            if(rb_indunayes.isChecked()){
                induname=et_induna_name.getText().toString();
                if(induname.isEmpty()){
                    et_induna_name.setError("this field required");
                    error=1;
                }
                else if(induname.length()<3){
                    et_induna_name.setError("Enter valid name");
                    et_induna_name.requestFocus();
                    error=1;
                }

            }else if(rb_indunano.isChecked()){
               induname="null";
            }
        }if(!prefer_yes.isChecked() && !prefer_no.isChecked()){
            prefer_no.setError("Select any one");
            error=1;
        }else if (!sim_yes.isChecked() && !sim_no.isChecked() ){
            sim_no.setError("Select any one");
            error=1;
        }else if(sim_yes.isChecked() || sim_no.isChecked()){
            if(sim_yes.isChecked()){
                mthandenisimcard=et_mthandenisimcard.getText().toString();
                order_new=null;
                if(mthandenisimcard.isEmpty()){
                    et_mthandenisimcard.setError("this field required");
                    error=1;
                }
            }else if(sim_no.isChecked()){
                RadioButton radioButton_no=rg_orderSim.findViewById(R.id.rb_orderno);
                if(rg_orderSim.getCheckedRadioButtonId()==-1){
                    radioButton_no.setError("Select any one");
                    error=1;
                }else {
                    radioButton_no.setError(null);
                }

            }
        } if(!refer_yes.isChecked() && !refer_no.isChecked()) {
            refer_no.setError("Select any one");
            error=1;
        }else if(refer_yes.isChecked()|| refer_no.isChecked()){
            if(refer_yes.isChecked()){
                referperson_name=et_refer_personnm.getText().toString();
                referperson_mob=et_refer_personno.getText().toString();
                refer_no.setError(null);
                Log.d("referd_etails","yes "+referperson_name+"\n"+referperson_mob);
                if(referperson_name.isEmpty()){
                    et_refer_personnm.setError("this field required");
                    error=1;
                    et_refer_personnm.requestFocus();
                }else if(referperson_mob.isEmpty()) {
                    et_refer_personno.setError("this field required");
                    et_refer_personno.requestFocus();
                    error=1;
                }
            }else if(refer_no.isChecked()){
                referperson_name="";
                referperson_mob="";
            }
        }

        /*if(branchnm.length()<3){
            et_branch_name.setError("Enter valid branch name");
            et_branch_name.requestFocus();
            error=1;
        }*/
        if(et_province.length()<3){
            et_province.setError("Enter valid province");
            et_province.requestFocus();
            error=1;
        }
        else if(!password.equals(confrmpaswrd)){
            et_confirm_password.setError("Password Doesn't match");
            et_confirm_password.requestFocus();
            error=1;
        }else if(password.length()<6){
            et_password.requestFocus();
            Toasty.warning(getApplicationContext(),"Password must be 6 digit long",Toasty.LENGTH_SHORT).show();
        }else {
            if(error>0){
                Log.d("error_value","error "+error);
            }else{
                register(fname,lname,mobile,email,address,password,induname,branchnm,province,txtdob,devicetype,devicetoken
                        ,like,prefer,simcard,mthandenisimcard,refer,referperson_name,referperson_mob,order_new,fav_song);
            }
        }

    }

    private void register(String fname, String lname, String mobile,String email, String address, String password, String induname, String branchnm, String province, String txtdob, String devicetype, String devicetoken, String like, String prefer, String simcard,String simcardno
    ,String refer,String personname,String personmob,String order_new,String fav_song) {
        progressDialog.show();
        if(branchnm==null || branchnm.isEmpty() || branchnm.equals("")){
            branchnm="null";
            Log.d("branchnm","check "+branchnm);
        }
        ApiService apiService= RetrofitClient.getClient().create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("first_name", fname);
            paramObject.put("last_name", lname);
            paramObject.put("mobile", mobile);
            paramObject.put("email", email);
            paramObject.put("address", address);
            paramObject.put("password", password);
            paramObject.put("induna_name", induname);
            paramObject.put("branch", branchnm);
            paramObject.put("province", province);
            paramObject.put("dob", birthdate);
            paramObject.put("device_type", devicetype);
            paramObject.put("device_token", devicetoken);
            paramObject.put("about_igcokama",like );
            paramObject.put("preference", prefer);
            paramObject.put("simCard", simcard);
            paramObject.put("order_new", order_new);
            paramObject.put("mthandeni_sim_number", simcardno);
            paramObject.put("have_reference", refer);
            paramObject.put("refer_name", personname);
            paramObject.put("refer_mobile", personmob);
            paramObject.put("fav_song",fav_song);
            RequestBody body = RequestBody.create(MediaType.parse("text/plain"), String.valueOf((paramObject)));
            Call<ResponseBody> call=apiService.register(body);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful() && response.code()==200){
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject=new JSONObject(response.body().string());
                            JSONObject dataObject=jsonObject.getJSONObject("data");
                            String message=jsonObject.getString("message");
                            String token=dataObject.getString("token");
                            Pref.putUserToken(getApplicationContext(),token);
                            Toasty.success(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), BioActivity.class));
                            //customType(ForgotPassActivity.this, "right-to-left");
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else if(response.code()==422){
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            JSONObject errorObject = jsonObject.getJSONObject("errors");
                            String message = errorObject.getString("email");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                    else{
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            String message = jsonObject.getString("error");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d(TAG,"error "+t.getMessage());
                    Toasty.error(RegisterActivity.this, t.getLocalizedMessage(), Toasty.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void dateofbirth() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = dayOfMonth + "/" + month + "/" + year;
                String monthstring = null,dayofmonthString = null;
                if (month < 10 && dayOfMonth < 10) {

                    monthstring = "0" + month;
                    dayofmonthString = "0" + dayOfMonth;
                    birthdate = year + "-" + monthstring + "-" + dayofmonthString + "T00:00:00.000Z";
                } else if (month < 10 && dayOfMonth > 10) {

                    monthstring = "0" + month;
                    dayofmonthString=""+dayOfMonth;
                    birthdate = year + "-" + monthstring + "-" + dayOfMonth + "T00:00:00.000Z";
                } else if (month > 10 && dayOfMonth < 10) {
                    dayofmonthString = "0" + dayOfMonth;
                    monthstring=""+month;
                    birthdate = year + "-" + month + "-" + dayofmonthString + "T00:00:00.000Z";
                } else {
                    monthstring=""+month;
                    dayofmonthString=""+dayOfMonth;
                    birthdate = year + "-" + month + "-" + dayOfMonth + "T00:00:00.000Z";
                }
                String stdate = year+"-"+monthstring + "-" + dayofmonthString;
                birthdate=stdate;
                dob.setText(stdate);
            }
        }, year, month, day);
        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        datePickerDialog.show();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(group.getId()==R.id.rg_prefer){
            if(checkedId!=-1){
                RadioButton rb=group.findViewById(checkedId);
                rb.setError(null);
                prefer=rb.getText().toString();
                Log.d(TAG,"radiobutton "+prefer);
            }else{
               prefer_no.setError("Select any one");
                rg_prefer.requestFocus();
            }
        }if(group.getId()==R.id.rg_sim){
            if(checkedId!=-1){
                RadioButton rb=group.findViewById(checkedId);
                rb.setError(null);
                simcard=rb.getText().toString();
                if(simcard.equalsIgnoreCase("Yes")){
                    et_mthandenisimcard.setVisibility(View.VISIBLE);
                    tvorderSim.setVisibility(View.GONE);
                    rg_orderSim.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) tvrefer.getLayoutParams();
                    params.addRule(RelativeLayout.BELOW,R.id.mthandenisimcard);
                }else{
                    mthandenisimcard="";
                    et_mthandenisimcard.setVisibility(View.GONE);
                    tvorderSim.setVisibility(View.VISIBLE);
                    rg_orderSim.setVisibility(View.VISIBLE);
                    RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) tvrefer.getLayoutParams();
                    params.addRule(RelativeLayout.BELOW,R.id.rg_ordersim);

                }
                Log.d(TAG,"radiobutton "+simcard);
            }
        }
        if(group.getId()==R.id.rg_ordersim){
            if (checkedId != -1) {
                RadioButton rb=group.findViewById(checkedId);
                rb.setError(null);
                order_new=rb.getText().toString();
                Log.d(TAG,"radiobutton "+order_new+checkedId);
            }
        }
        if(group.getId()==R.id.rg_refer){
            if(checkedId!=-1){
                RadioButton rb=group.findViewById(checkedId);
                rb.setError(null);
                refer=rb.getText().toString();
                if(refer.equalsIgnoreCase("Yes")){
                    et_refer_personnm.setVisibility(View.VISIBLE);
                    et_refer_personno.setVisibility(View.VISIBLE);
                }else{
                    referperson_name="";
                    referperson_mob="";
                    et_refer_personnm.setVisibility(View.GONE);
                    et_refer_personno.setVisibility(View.GONE);
                }
                Log.d(TAG,"radiobutton "+refer+checkedId);
            }
        }
        if(group.getId()==R.id.rg_induna){
            if(checkedId!=-1){
                RadioButton rb=group.findViewById(checkedId);
                rb.setError(null);
                if(rb.getId()==R.id.rb_indunayes)
                {
                    et_induna_name.setVisibility(View.VISIBLE);
                }else{
                    induname="null";
                    et_induna_name.setVisibility(View.GONE);
                }
            }
        }
    }
}
