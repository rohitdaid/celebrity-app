package com.app.mthandenimanqele.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.mthandenimanqele.NetworkError;
import com.app.mthandenimanqele.NetworkStateReceiver;
import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.adapter.Listall_photos_RecyclerView_Adapter;
import com.app.mthandenimanqele.adapter.Listall_videos_RecyclerView_Adapter;
import com.app.mthandenimanqele.webservices.BaseUrl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class New_BioActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.data)
    TextView datatype;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    PostFragment adapter;
    Resources res;
    Listall_videos_RecyclerView_Adapter videos_recyclerView_adapter;
    Listall_photos_RecyclerView_Adapter photos_recycleView_adapter;
    List<String> photosCollection=new ArrayList<>();
    List<String> videosCollection=new ArrayList<>();
    NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new__bio);
        ButterKnife.bind(this);
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        res=getResources();
        setData();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /*mViewPager = (ViewPager) findViewById(R.id.view_pager);
        adapter = new PostFragment(getSupportFragmentManager());
        adapter.addFragment(new ImagesFragment(), "Images");
        adapter.addFragment(new VideosFragment(), "Videos");
        mViewPager.setAdapter(adapter);
        mViewPager.setCurrentItem(0);
        adapter.notifyDataSetChanged();*/

    }

    private void setData(){
        String[] photos=res.getStringArray(R.array.images);
        String[] videos=res.getStringArray(R.array.videos);
        for (int i=0;i<photos.length;i++){
            photosCollection.add(i, BaseUrl.imageurl+photos[i]);
        }
        Intent i=getIntent();
        int value=i.getIntExtra("value",0);
        videosCollection.addAll(Arrays.asList(videos));

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager1);
        if(value==0){
            datatype.setText("Latest Photos");
            photos_recycleView_adapter = new Listall_photos_RecyclerView_Adapter(New_BioActivity.this,BioActivity.imageslist);
            recyclerView.setAdapter(photos_recycleView_adapter);
        }else{
            datatype.setText("Latest Videos");
            videos_recyclerView_adapter = new Listall_videos_RecyclerView_Adapter(New_BioActivity.this,BioActivity.videoslist);
            recyclerView.setAdapter(videos_recyclerView_adapter);
            Log.d("images_llist","values "+BioActivity.videoslist);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {
        Intent i=new Intent(this, NetworkError.class);
        startActivity(i);
    }

}
