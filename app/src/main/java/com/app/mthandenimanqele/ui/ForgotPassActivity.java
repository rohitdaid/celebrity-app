package com.app.mthandenimanqele.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.webservices.ApiService;
import com.app.mthandenimanqele.webservices.RetrofitClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.btn_verifycode)
    Button verifycode;

    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.tv_to_register)
    TextView register;

    String email;
    ProgressDialog progressDialog;
    private String TAG=getClass().getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        ButterKnife.bind(this);
        progressDialog=new ProgressDialog(this);
        verifycode.setOnClickListener(this);
        register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btn_verifycode){
            email=et_email.getText().toString();
            if(email.isEmpty()){
                Toasty.warning(getApplicationContext(),"Enter email",Toasty.LENGTH_SHORT).show();
            }else{
                forgotpassword(email);
            }
        }if(v.getId()==R.id.tv_to_register){
            startActivity(new Intent(this,RegisterActivity.class));
            finish();
        }
    }

    private void forgotpassword(String email) {
        progressDialog.show();
        ApiService apiService= RetrofitClient.getClient().create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("email", email);
            RequestBody body = RequestBody.create(MediaType.parse("text/plain"), String.valueOf((paramObject)));
            Call<ResponseBody> call=apiService.forgetPassword(body);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful() && response.code()==200){
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject=new JSONObject(response.body().string());
                            String message=jsonObject.getString("message");
                            Toasty.success(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), VerifyOtpActivity.class)
                                    .putExtra("email",email));
                            //customType(ForgotPassActivity.this, "right-to-left");
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else if(response.code()==422){
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            JSONObject errorObject = jsonObject.getJSONObject("errors");
                            String message = errorObject.getString("email");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                    else{
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response.errorBody().string());
                            String message = jsonObject.getString("error_description");
                            Toasty.warning(getApplicationContext(),message,Toasty.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d(TAG,"error "+t.getMessage());
                    Toasty.error(ForgotPassActivity.this, t.getLocalizedMessage(), Toasty.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
