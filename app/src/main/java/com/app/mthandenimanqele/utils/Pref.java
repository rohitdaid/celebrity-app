package com.app.mthandenimanqele.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Pref {
    public static final String USER_FIRSTNAME = "user_fname";
    public static final String USER_LASTNAME = "user_lname";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_MOBILE = "user_mobile";
    public static final String USER_TOKEN = "user_token";
    public static final String USER_ID = "user_id";
    public static final String USER_INSTAGRAM = "user_insta";
    public static final String USER_FB = "user_fb";
    public static final String USER_TWITTER = "user_twitter";
    public static final String USER_DOB = "user_dob";
    public static final String USER_Address = "user_address";
    public static final String USER_BRANCH = "user_branch";
    public static final String USER_PROVINCE = "user_province";
    public static final String Induna_Name = "induna_name";
    public static final String About_Igcokama = "about_igcokama";
    public static final String USER_preference = "user_preference";
    public static final String USER_DP = "user_dp";
    public static SharedPreferences getPref(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void putUserToken(Context context,String token){
        getPref(context).edit().putString(USER_TOKEN,token).commit();
    }
    public static void clearPref(Context context) {
        getPref(context).edit().clear().commit();
    }
    public static String getUserToken(Context context) {
        return getPref(context).getString(USER_TOKEN, null);
    }

    public static String getUserInstagram(Context context) {
        return getPref(context).getString(USER_INSTAGRAM, null);
    }

    public static String getUserFb(Context context) {
        return getPref(context).getString(USER_FB, null);
    }

    public static String getUserTwitter(Context context) {
        return getPref(context).getString(USER_TWITTER, null);
    }

    public static void putUserInstagram(Context context,String token){
        getPref(context).edit().putString(USER_INSTAGRAM,token).commit();
    }
    public static void putUserFb(Context context,String token){
        getPref(context).edit().putString(USER_FB,token).commit();
    }
    public static void putUserTwitter(Context context,String token){
        getPref(context).edit().putString(USER_TWITTER,token).commit();
    }

    public static void putUserFirstname(Context context,String token) {
        getPref(context).edit().putString(USER_FIRSTNAME,token).commit();
    }

    public static void putUserLastname(Context context,String token) {
        getPref(context).edit().putString(USER_LASTNAME,token).commit();
    }

    public static void putUserEmail(Context context,String token) {
        getPref(context).edit().putString(USER_EMAIL,token).commit();
    }

    public static void putUserMobile(Context context,String token) {
        getPref(context).edit().putString(USER_MOBILE,token).commit();
    }
    public static void putUserDOB(Context context,String token) {
        getPref(context).edit().putString(USER_DOB,token).commit();
    }
    public static void putUSER_Address(Context context,String token) {
        getPref(context).edit().putString(USER_Address,token).commit();
    }
    public static void putUSER_BRANCH(Context context,String token) {
        getPref(context).edit().putString(USER_BRANCH,token).commit();
    }
    public static void putUSER_PROVINCE(Context context,String token) {
        getPref(context).edit().putString(USER_PROVINCE,token).commit();
    }
    public static void putInduna_Name(Context context,String token) {
        getPref(context).edit().putString(Induna_Name,token).commit();
    }
    public static void putAbout_Igcokama(Context context,String token) {
        getPref(context).edit().putString(About_Igcokama,token).commit();
    }
    public static void putUSER_preference(Context context,String token) {
        getPref(context).edit().putString(USER_preference,token).commit();
    }
    public static void putUSER_DP(Context context,String token) {
        getPref(context).edit().putString(USER_DP,token).commit();
    }

    public static String getUserFirstname(Context context) {
        return getPref(context).getString(USER_FIRSTNAME, null);
    }

    public static String getUserLastname(Context context) {
        return getPref(context).getString(USER_LASTNAME, null);
    }

    public static String getUserEmail(Context context) {
        return getPref(context).getString(USER_EMAIL, null);
    }

    public static String getUserMobile(Context context) {
        return getPref(context).getString(USER_MOBILE, null);
    }
    public static String getUserDOB(Context context) {
        return getPref(context).getString(USER_DOB, null);
    }
    public static String getUSER_Address(Context context) {
        return getPref(context).getString(USER_Address, null);
    }
    public static String getUSER_BRANCH(Context context) {
        return getPref(context).getString(USER_BRANCH, null);
    }
    public static String getUSER_PROVINCE(Context context) {
        return getPref(context).getString(USER_PROVINCE, null);
    }
    public static String getInduna_Name(Context context) {
        return getPref(context).getString(Induna_Name, null);
    }
    public static String getAbout_Igcokama(Context context) {
        return getPref(context).getString(About_Igcokama, null);
    }
    public static String getUSER_preference(Context context) {
        return getPref(context).getString(USER_preference, null);
    }
    public static String getUSER_DP(Context context) {
        return getPref(context).getString(USER_DP, null);
    }

}
