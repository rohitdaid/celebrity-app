package com.app.mthandenimanqele.webservices;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiService {
    @Headers("Content-Type: application/json")
    @POST("auth/login")
    Call<ResponseBody> login(@Body RequestBody login);

    @Headers("Content-Type: application/json")
    @POST("auth/forgetPassword")
    Call<ResponseBody> forgetPassword(@Body RequestBody forgetPassword);

    @Headers("Content-Type: application/json")
    @GET("auth/logout")
    Call<ResponseBody> logout(@Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @POST("auth/verifyToken")
    Call<ResponseBody> verifyotp (@Body RequestBody verifyotp);

    @Headers("Content-Type: application/json")
    @POST("auth/resetPassword")
    Call<ResponseBody> resetpassword (@Body RequestBody resetpassword);

    @Headers("Content-Type: application/json")
    @POST("auth/register")
    Call<ResponseBody> register (@Body RequestBody register);

    @Headers("Content-Type: application/json")
    @POST("updateProfile")
    Call<ResponseBody> updateprofile (@Body RequestBody updateprofile,@Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET("profile")
    Call<ResponseBody> profile (@Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET("media/video")
    Call<ResponseBody> videoslist (@Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET("media/image")
    Call<ResponseBody> imageslist (@Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET("musicList/1")
    Call<ResponseBody> songslist (@Header("Authorization") String token);

    @GET("ads")
    Call<ResponseBody> advertisement (@Header("Authorization") String token);
}
