package com.app.mthandenimanqele.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.constants.Constants;
import com.app.mthandenimanqele.ui.musicplayer.MusicPlayer;

public class MusicPlayerService extends Service {
    private Notification notification;
    private String song;
    MusicPlayer musicPlayer;
    private RemoteViews views;
    public static final String ACTION_PLAY = "ACTION_PLAY";
    public static final String ACTION_PAUSE = "ACTION_PAUSE";
    public static final String ACTION_PREVIOUS = "ACTION_PREVIOUS";
    public static final String ACTION_NEXT = "ACTION_NEXT";
    public static final String ACTION_STOP = "ACTION_STOP";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //needs to be initialized
        if(intent.getAction().equals(Constants.ACTION.STARTFOREGROUND_ACTION)){
            song =intent.getStringExtra("song");
            shownotification(PlaybackStatus.PLAYING);
        }
        handleIncomingActions(intent);
        return START_STICKY;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void shownotification(PlaybackStatus playbackStatus) {
        views= new RemoteViews(getPackageName(),
                R.layout.status_bar);
        views.setViewVisibility(R.id.pause, View.VISIBLE);
        views.setViewVisibility(R.id.play, View.GONE);
        Intent playIntent = new Intent(this, MusicPlayerService.class);
        playIntent.setAction(ACTION_PAUSE);
        PendingIntent pplayIntent = PendingIntent.getService(this, 0,
                playIntent, 0);
        views.setOnClickPendingIntent(R.id.pause,pplayIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(this, MusicPlayer.class);
        notificationIntent.putExtra("song",song);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            Notification.Builder builder = new Notification.Builder(this);
            notification = builder.setSmallIcon(R.drawable.logo)
                    .setContent(views)
                    .setAutoCancel(false)
                    .setContentIntent(contentIntent)
                    .build();
            builder.setChannelId("CHANNEL_ID");
            NotificationChannel channel = new NotificationChannel(
                    "CHANNEL_ID",
                    "NotificationDemo",
                    NotificationManager.IMPORTANCE_HIGH
            );

        }

        else {
            Notification.Builder builder = new Notification.Builder(this);
            notification = builder.setSmallIcon(R.drawable.logo)
                    .setContent(views)
                    .setAutoCancel(false)
                    .setContentIntent(contentIntent)
                    .build();
            builder.setChannelId("CHANNEL_ID");
            NotificationChannel channel = new NotificationChannel(
                    "CHANNEL_ID",
                    "NotificationDemo",
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0, notification);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private enum  PlaybackStatus {
        PLAYING,
        PAUSED
    }
    private PendingIntent playbackAction(int actionNumber) {
        Intent playbackAction = new Intent(this, MusicPlayerService.class);
        Log.d("notification_service","actionNumber "+actionNumber);
        switch (actionNumber) {
            case 0:
                // Pause
                views.setViewVisibility(R.id.pause, View.GONE);
                views.setViewVisibility(R.id.play, View.VISIBLE);
                playbackAction.setAction(ACTION_PAUSE);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 1:
                // Play
                views.setViewVisibility(R.id.pause, View.VISIBLE);
                views.setViewVisibility(R.id.play, View.GONE);
                playbackAction.setAction(ACTION_PLAY);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 2:
                // Next track
                playbackAction.setAction(ACTION_PREVIOUS);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 3:
                // Previous track
                playbackAction.setAction(ACTION_NEXT);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            default:
                break;
        }
        return null;
    }

    private void handleIncomingActions(Intent playbackAction) {
        if (playbackAction == null || playbackAction.getAction() == null) return;
        Log.d("notification_service","action"+playbackAction);
        String actionString = playbackAction.getAction();
        if (actionString.equalsIgnoreCase(ACTION_PLAY)) {
            //transportControls.play();
        } else if (actionString.equalsIgnoreCase(ACTION_PAUSE)) {
            //transportControls.pause();
        } else if (actionString.equalsIgnoreCase(ACTION_NEXT)) {
            //transportControls.skipToNext();
        } else if (actionString.equalsIgnoreCase(ACTION_PREVIOUS)) {
            //transportControls.skipToPrevious();
        } else if (actionString.equalsIgnoreCase(ACTION_STOP)) {
            //transportControls.stop();
        }
    }

}
