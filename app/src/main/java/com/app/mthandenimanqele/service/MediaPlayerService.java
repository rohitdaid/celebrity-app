package com.app.mthandenimanqele.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.session.MediaSessionManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.ui.musicplayer.MusicPlayer;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MediaPlayerService extends Service implements AudioManager.OnAudioFocusChangeListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnSeekCompleteListener {
    private Notification notification;
    private String song;
    public static MediaPlayer mediaPlayer;
    private String mediaFile;
    public static int resumePosition;
    private AudioManager audioManager;
    //Handle incoming phone calls
    private boolean ongoingCall = false;
    private PhoneStateListener phoneStateListener;
    private TelephonyManager telephonyManager;
    //List of available Audio files
    //private ArrayList<MediaStore.Audio> audioList;
    private List<String> audioList;
    private int audioIndex = -1;
    public MusicPlayer musicPlayer;
    //private MediaStore.Audio activeAudio; //an object of the currently playing audio
    private String activeAudio;
    private RemoteViews views;
    public static final String ACTION_PLAY = "ACTION_PLAY";
    public static final String ACTION_PAUSE = "ACTION_PAUSE";
    public static final String ACTION_PREVIOUS = "ACTION_PREVIOUS";
    public static final String ACTION_NEXT = "ACTION_NEXT";
    public static final String ACTION_STOP = "ACTION_STOP";
    private MediaSessionManager mediaSessionManager;
    private MediaSessionCompat mediaSession;
    private MediaControllerCompat.TransportControls transportControls;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    private BroadcastReceiver pausemusicat_value = new BroadcastReceiver() {
        /** Receives the broadcast that has been fired */
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction()==MusicPlayer.Broadcast_PAUSE_AUDIO){
                String pausedmusic_value =intent.getStringExtra("pauseat");
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    resumePosition = mediaPlayer.getCurrentPosition();
                   /* musicPlayer.pause.setVisibility(View.GONE);
                    musicPlayer.play.setVisibility(View.VISIBLE);*/
                    shownotification(PlaybackStatus.PAUSED);
                }
            }
        }
    };
    private BroadcastReceiver resumemusicat_value = new BroadcastReceiver() {
        /** Receives the broadcast that has been fired */
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction()==MusicPlayer.Broadcast_RESUME_AUDIO){
                String pausedmusic_value =intent.getStringExtra("resumeeat");
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.seekTo(resumePosition);
                    /*musicPlayer.pause.setVisibility(View.VISIBLE);
                    musicPlayer.play.setVisibility(View.GONE);*/
                    mediaPlayer.start();
                    new SeekBarHandler().execute();
                    shownotification(PlaybackStatus.PLAYING);
                }
            }
        }
    };

    //AudioPlayer notification ID
    private static final int NOTIFICATION_ID = 101;
    private final IBinder iBinder = new LocalBinder();
    private NotificationManager notificationManager;

    @Override
    public void onSeekComplete(MediaPlayer mp) {

    }

    public class LocalBinder extends Binder {
        public MediaPlayerService getService() {
            return MediaPlayerService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }
    @Override
    public boolean onUnbind(Intent intent){
        //player.stop();
        //player.release();
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //needs to be initialized
        try {
            //Load data from SharedPreferences
            //audioList = musicPlayer.loadAudio();
            audioList = musicPlayer.songslist;
            preferences = getSharedPreferences(MusicPlayer.STORAGE, Context.MODE_PRIVATE);
            editor=preferences.edit();
            audioIndex= preferences.getInt("audioIndex", 0);//return -1 if no data found
            // Log.d("chkaudio","audioIndex "+audioIndex);
            if (audioIndex != -1 && audioIndex < audioList.size()) {
                //index is in a valid range
                Log.d("notification_service", "size " + audioList.size());
                activeAudio = audioList.get(audioIndex);
            } else {
                stopSelf();
                Log.d("notification_service", "else ");
            }
        } catch (NullPointerException e) {
            Log.d("notification_service", "catch  " + e.getLocalizedMessage());
            stopSelf();
        }

        //Request audio focus
        if (requestAudioFocus() == false) {
            //Could not gain focus
            stopSelf();
        }

        if (mediaSessionManager == null) {
            try {
                initMediaSession();
                initMediaPlayer();
            } catch (RemoteException e) {
                e.printStackTrace();
                stopSelf();
            }
            shownotification(PlaybackStatus.PLAYING);
        }

        //Handle Intent action from MediaSession.TransportControls
        handleIncomingActions(intent);
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Perform one-time setup procedures

        // Manage incoming phone calls during playback.
        // Pause MediaPlayer on incoming call,
        // Resume on hangup.
        callStateListener();
        //ACTION_AUDIO_BECOMING_NOISY -- change in audio outputs -- BroadcastReceiver
        registerBecomingNoisyReceiver();
        //Listen for new Audio to play -- BroadcastReceiver
        register_playNewAudio();
        register_pauseAudio();
        register_resumeAudio();

    }

    private void initMediaPlayer() {
        mediaPlayer = new MediaPlayer();
        //Set up MediaPlayer event listeners
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnErrorListener(this);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnSeekCompleteListener(this);
        /*mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnInfoListener(this);*/
        //Reset so that the MediaPlayer is not pointing to another data source
        mediaPlayer.reset();

        // mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            // Set the data source to the mediaFile location
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mediaPlayer.setAudioAttributes(new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .setLegacyStreamType(AudioManager.STREAM_MUSIC)
                        .build());
            } else {
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            }
            mediaPlayer.setDataSource(activeAudio);

            //mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
            stopSelf();
        }
        mediaPlayer.prepareAsync();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void shownotification(PlaybackStatus playbackStatus) {
        Log.d("notification_service","shownotification");
        views = new RemoteViews(getPackageName(),
                R.layout.status_bar);
        String text=MusicPlayer.songnamelist.get(audioIndex);
        views.setTextViewText(R.id.music_name,text);
        if (playbackStatus.equals(PlaybackStatus.PAUSED)) {
            views.setViewVisibility(R.id.pause, View.GONE);
            views.setViewVisibility(R.id.play, View.VISIBLE);
        } else {
            views.setViewVisibility(R.id.pause, View.VISIBLE);
            views.setViewVisibility(R.id.play, View.GONE);
        }
        Intent pauseIntent = new Intent(this, MediaPlayerService.class);
        pauseIntent.setAction(ACTION_PAUSE);
        PendingIntent ppauseIntent = PendingIntent.getService(this, 0,
                pauseIntent, 0);
        views.setOnClickPendingIntent(R.id.pause, ppauseIntent);

        Intent playIntent = new Intent(this, MediaPlayerService.class);
        playIntent.setAction(ACTION_PLAY);
        PendingIntent pplayIntent = PendingIntent.getService(this, 0,
                playIntent, 0);
        views.setOnClickPendingIntent(R.id.play, pplayIntent);

        Intent previousIntent = new Intent(this, MediaPlayerService.class);
        previousIntent.setAction(ACTION_PREVIOUS);
        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0,
                previousIntent, 0);
        views.setOnClickPendingIntent(R.id.previous, ppreviousIntent);

        Intent nextIntent = new Intent(this, MediaPlayerService.class);
        nextIntent.setAction(ACTION_NEXT);
        PendingIntent pnextIntent = PendingIntent.getService(this, 0,
                nextIntent, 0);
        views.setOnClickPendingIntent(R.id.next, pnextIntent);

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(this, MusicPlayer.class);
        notificationIntent.putExtra("song", song);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder builder = new Notification.Builder(this);
            notification = builder.setSmallIcon(R.drawable.logo)
                    .setContent(views)
                    .setAutoCancel(false)
                    .setContentIntent(contentIntent)
                    .build();
            /*notification.flags |=Notification.FLAG_NO_CLEAR;
            notification.flags |=Notification.FLAG_ONGOING_EVENT;*/
            builder.setChannelId("CHANNEL_ID");
            NotificationChannel channel = new NotificationChannel(
                    "CHANNEL_ID",
                    "NotificationDemo",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            notificationManager.createNotificationChannel(channel);
        } else {
            Notification.Builder builder = new Notification.Builder(this);
            notification = builder.setSmallIcon(R.drawable.logo)
                    .setContent(views)
                    .setAutoCancel(false)
                    .setContentIntent(contentIntent)
                    .build();
            /*notification.flags |=Notification.FLAG_NO_CLEAR;
            notification.flags |=Notification.FLAG_ONGOING_EVENT;*/
            builder.setChannelId("CHANNEL_ID");
            NotificationChannel channel = new NotificationChannel(
                    "CHANNEL_ID",
                    "NotificationDemo",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0, notification);

    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onAudioFocusChange(int focusChange) {
        //Invoked when the audio focus of the system is updated.
        Log.d("mediaplayer_service ","focusChange "+focusChange);
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                if (mediaPlayer == null) initMediaPlayer();
                else if (!mediaPlayer.isPlaying()) mediaPlayer.start();
                mediaPlayer.setVolume(1.0f, 1.0f);
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                // Lost focus for an unbounded amount of time: stop playback and release media player
                if (mediaPlayer.isPlaying()) pauseMedia();
                shownotification(PlaybackStatus.PAUSED);

                /*mediaPlayer.release();
                mediaPlayer = null;*/
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (mediaPlayer.isPlaying()) mediaPlayer.pause();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Lost focus for a short time, but it's ok to keep playing
                // at an attenuated level
                if (mediaPlayer.isPlaying()) mediaPlayer.setVolume(0.1f, 0.1f);
                break;
        }
    }

    private boolean requestAudioFocus() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            //Focus gained
            return true;
        }
        //Could not gain focus
        return false;
    }

    private boolean removeAudioFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED ==
                audioManager.abandonAudioFocus(this);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
//Invoked when playback of a media source has completed.
        stopMedia();
        //stop the service
        stopSelf();
        skipToNext();

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        //Invoked when there has been an error during an asynchronous operation
        switch (what) {
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                Log.d("MediaPlayer Error", "MEDIA ERROR NOT VALID FOR PROGRESSIVE PLAYBACK " + extra);
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                Log.d("MediaPlayer Error", "MEDIA ERROR SERVER DIED " + extra);
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                Log.d("MediaPlayer Error", "MEDIA ERROR UNKNOWN " + extra);
                break;
        }
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        playMedia();
    }


    private enum PlaybackStatus {
        PLAYING,
        PAUSED
    }

    private PendingIntent playbackAction(int actionNumber) {
        Intent playbackAction = new Intent(this, MediaPlayerService.class);
        Log.d("notification_service", "actionNumber " + actionNumber);
        switch (actionNumber) {
            case 0:
                // Pause
                views.setViewVisibility(R.id.pause, View.GONE);
                views.setViewVisibility(R.id.play, View.VISIBLE);
                playbackAction.setAction(ACTION_PAUSE);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 1:
                // Play
                views.setViewVisibility(R.id.pause, View.VISIBLE);
                views.setViewVisibility(R.id.play, View.GONE);
                playbackAction.setAction(ACTION_PLAY);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 2:
                // Next track
                playbackAction.setAction(ACTION_PREVIOUS);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 3:
                // Previous track
                playbackAction.setAction(ACTION_NEXT);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            default:
                break;
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void handleIncomingActions(Intent playbackAction) {
        if (playbackAction == null || playbackAction.getAction() == null) return;
        Log.d("notification_service", "action" + playbackAction);
        String actionString = playbackAction.getAction();
        if (actionString.equalsIgnoreCase(ACTION_PLAY)) {
            transportControls.play();
            views.setViewVisibility(R.id.pause, View.VISIBLE);
            views.setViewVisibility(R.id.play, View.GONE);
        } else if (actionString.equalsIgnoreCase(ACTION_PAUSE)) {
            transportControls.pause();
            shownotification(PlaybackStatus.PAUSED);
        } else if (actionString.equalsIgnoreCase(ACTION_NEXT)) {
            transportControls.skipToNext();
        } else if (actionString.equalsIgnoreCase(ACTION_PREVIOUS)) {
            transportControls.skipToPrevious();
        } else if (actionString.equalsIgnoreCase(ACTION_STOP)) {
            transportControls.stop();
        }
    }


    private void playMedia() {
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
            musicPlayer.pause.setVisibility(View.VISIBLE);
            musicPlayer.play.setVisibility(View.GONE);
            new SeekBarHandler().execute();
        }
    }

    private void stopMedia() {
        if (mediaPlayer == null) return;
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
    }

    private void pauseMedia() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            resumePosition = mediaPlayer.getCurrentPosition();
            musicPlayer.pause.setVisibility(View.GONE);
            musicPlayer.play.setVisibility(View.VISIBLE);
        }
    }

    private void resumeMedia() {
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.seekTo(resumePosition);
            musicPlayer.pause.setVisibility(View.VISIBLE);
            musicPlayer.play.setVisibility(View.GONE);
            mediaPlayer.start();
            new SeekBarHandler().execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            stopMedia();
            mediaPlayer.release();
            stopForeground(true);
        }
        removeAudioFocus();
        //Disable the PhoneStateListener
        if (phoneStateListener != null) {
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }

        removeNotification();

        //unregister BroadcastReceivers
        unregisterReceiver(becomingNoisyReceiver);
        unregisterReceiver(playNewAudio);
        unregisterReceiver(resumemusicat_value);
        unregisterReceiver(pausemusicat_value);
        //clear cached playlist
        //  new StorageUtil(getApplicationContext()).clearCachedAudioPlaylist();
    }

    //Becoming noisy
    private BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onReceive(Context context, Intent intent) {
            //pause audio on ACTION_AUDIO_BECOMING_NOISY
            pauseMedia();
            shownotification(PlaybackStatus.PAUSED);
        }
    };

    private void registerBecomingNoisyReceiver() {
        //register after getting audio focus
        IntentFilter intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        registerReceiver(becomingNoisyReceiver, intentFilter);
    }

    private BroadcastReceiver playNewAudio = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onReceive(Context context, Intent intent) {

            //Get the new media index form SharedPreferences
            audioIndex= preferences.getInt("audioIndex", 0);//return -1 if no data found

            if (audioIndex != -1 && audioIndex < audioList.size()) {
                //index is in a valid range
                activeAudio = audioList.get(audioIndex);
            } else {
                stopSelf();
            }
            Log.d("chkaudio","index "+audioIndex+":"+activeAudio);
            //A PLAY_NEW_AUDIO action received
            //reset mediaPlayer to play the new Audio
            stopMedia();
            mediaPlayer.reset();
            initMediaPlayer();
            updateMetaData();
            shownotification(PlaybackStatus.PLAYING);
        }
    };

    private void register_playNewAudio() {
        //Register playNewMedia receiver
        IntentFilter filter = new IntentFilter(MusicPlayer.Broadcast_PLAY_NEW_AUDIO);
        registerReceiver(playNewAudio, filter);
    }
    private void register_pauseAudio() {
        //Register playNewMedia receiver
        IntentFilter filter = new IntentFilter(MusicPlayer.Broadcast_PAUSE_AUDIO);
        registerReceiver(pausemusicat_value, filter);
    }private void register_resumeAudio() {
        //Register playNewMedia receiver
        IntentFilter filter = new IntentFilter(MusicPlayer.Broadcast_RESUME_AUDIO);
        registerReceiver(resumemusicat_value, filter);
    }

    //Handle incoming phone calls
    private void callStateListener() {
        // Get the telephony manager
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        //Starting listening for PhoneState changes
        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state) {
                    //if at least one call exists or the phone is ringing
                    //pause the MediaPlayer
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                    case TelephonyManager.CALL_STATE_RINGING:
                        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                            pauseMedia();
                            ongoingCall = true;
                        }
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        // Phone idle. Start playing.
                        if (mediaPlayer != null ) {
                            if (ongoingCall) {
                                ongoingCall = false;
                                //resumeMedia();
                            }
                        }
                        break;
                }
            }
        };
        // Register the listener with the telephony manager
        // Listen for changes to the device call state.
        telephonyManager.listen(phoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);
    }

    private void initMediaSession() throws RemoteException {
        if (mediaSessionManager != null) return; //mediaSessionManager exists

        mediaSessionManager = (MediaSessionManager) getSystemService(Context.MEDIA_SESSION_SERVICE);
        // Create a new MediaSession
        mediaSession = new MediaSessionCompat(getApplicationContext(), "AudioPlayer");
        //Get MediaSessions transport controls
        transportControls = mediaSession.getController().getTransportControls();
        //set MediaSession -> ready to receive media commands
        mediaSession.setActive(true);
        //indicate that the MediaSession handles transport control commands
        // through its MediaSessionCompat.Callback.
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);

        //Set mediaSession's MetaData
        updateMetaData();

        // Attach Callback to receive MediaSession updates
        mediaSession.setCallback(new MediaSessionCompat.Callback() {
            // Implement callbacks
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onPlay() {
                super.onPlay();
                resumeMedia();
                shownotification(PlaybackStatus.PLAYING);
            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onPause() {
                super.onPause();
                pauseMedia();
                shownotification(PlaybackStatus.PAUSED);
            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onSkipToNext() {
                super.onSkipToNext();
                skipToNext();
                updateMetaData();
                shownotification(PlaybackStatus.PLAYING);
            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onSkipToPrevious() {
                super.onSkipToPrevious();
                skipToPrevious();
                updateMetaData();
                shownotification(PlaybackStatus.PLAYING);
            }

            @Override
            public void onStop() {
                super.onStop();
                removeNotification();
                //Stop the service
                stopSelf();
            }

            @Override
            public void onSeekTo(long position) {
                super.onSeekTo(position);
                Log.d("notification_service", "seekposition " + position);
            }
        });
    }

    private void updateMetaData() {
       /* Bitmap albumArt = BitmapFactory.decodeResource(getResources(),
                R.drawable.mm); //replace with medias albumArt
        // Update the current metadata
        mediaSession.setMetadata(new MediaMetadataCompat.Builder()
                .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, albumArt)
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, activeAudio.getArtist())
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, activeAudio.getAlbum())
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, activeAudio.getTitle())
                .build());*/
    }

    private void skipToNext() {

        if (audioIndex == audioList.size()-1) {
            //if last in playlist
            audioIndex = 0;
            activeAudio = audioList.get(audioIndex);

        } else {
            //get next in playlist
            audioIndex=audioIndex+1;
            activeAudio = audioList.get(audioIndex);
        }
        Log.d("chkaudio ","data "+audioIndex+":"+activeAudio);
        editor.putInt("audioIndex",audioIndex);
        editor.putString("songname",musicPlayer.songnamelist.get(audioIndex));
        editor.putString("artistname",musicPlayer.artistlist.get(audioIndex));
        musicPlayer.musicname.setText(musicPlayer.songnamelist.get(audioIndex));
        musicPlayer.musicdesc.setText(musicPlayer.artistlist.get(audioIndex));
        editor.apply();
        //Update stored index
        //  new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);

        stopMedia();
        //reset mediaPlayer
        mediaPlayer.reset();
        initMediaPlayer();

    }

    private void skipToPrevious() {

        if (audioIndex == 0) {
            //if first in playlist
            //set index to the last of audioList
            audioIndex = audioList.size() - 1;
            activeAudio = audioList.get(audioIndex);
        } else {
            //get previous in playlist
            activeAudio = audioList.get(--audioIndex);
        }
        //Update stored index
        //new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);

        stopMedia();
        //reset mediaPlayer
        mediaPlayer.reset();
        initMediaPlayer();
    }


    private void removeNotification() {
        notificationManager.cancel(0);
        notificationManager.cancelAll();
    }

    public class SeekBarHandler extends AsyncTask<Void, Void, Void> {

        private long mins, secs;

        @Override
        protected void onPostExecute(Void result) {
            //Log.d("##########Seek Bar Handler ################","###################Destroyed##################");
            super.onPostExecute(result);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            int dividetym = mediaPlayer.getDuration() / 100;
            musicPlayer.dividetym=dividetym;
            long seconds = 0;
            Log.d("error_chk","onProgressUpdate "+musicPlayer.dividetym);
            if(dividetym!=0){

                //musicPlayer.musictime.setText(mediaPlayer.getDuration());
                editor.apply();
                int progress = mediaPlayer.getCurrentPosition() / musicPlayer.dividetym;
                seconds = TimeUnit.MILLISECONDS.toSeconds(mediaPlayer.getCurrentPosition());
                musicPlayer.seekbar.setProgress(progress);
            }

            mins = seconds / 60;
            seconds = seconds - mins * 60;
            secs = seconds;

            super.onProgressUpdate(values);
            long finalSeconds = seconds;
            /*musicPlayer.runOnUiThread(new Runnable() {*/
                /*@Override
                public void run() {*/
            String time = mins + ":" + secs;
            if (mins < 10 && secs < 10) {
                time = "0" + mins + ":" + "0" + secs;
            } else if (mins < 10) {
                time = "0" + mins + ":" + secs;
            } else if (secs < 10) {
                time = mins + ":" + "0" + secs;
            }
            /*musicPlayer.currentMusictime.setText(time);*/

            Intent intentFilter=new Intent(MusicPlayer.Broadcast_SeekBar_Update);
            intentFilter.putExtra("seekvalue",time);
            musicPlayer.duration=mediaPlayer.getDuration();
            musicPlayer.mpcurrentposition=mediaPlayer.getCurrentPosition();
            intentFilter.putExtra("musictime",mediaPlayer.getDuration());
            //Log.d("getmusictym", "progress " + mediaPlayer.getDuration());
            intentFilter.putExtra("currentposition",mediaPlayer.getCurrentPosition());
            sendBroadcast(intentFilter);

            /*   }*/
            /*});*/
        }

        @SuppressLint("WrongThread")
        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                while (mediaPlayer.isPlaying()) {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    onProgressUpdate();
                }

            } catch (IllegalStateException e) {

            }
            return null;
        }
    }

}