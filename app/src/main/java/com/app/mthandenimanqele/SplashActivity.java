package com.app.mthandenimanqele;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.app.mthandenimanqele.R;
import com.app.mthandenimanqele.ui.BioActivity;
import com.app.mthandenimanqele.ui.LoginActivity;
import com.app.mthandenimanqele.utils.Pref;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_SCREEN_TIME_OUT=1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(Pref.getUserToken(getApplicationContext())!=null){
                    startActivity(new Intent(getApplicationContext(), BioActivity.class));
                    finish();
                }else{
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }

            }
        },SPLASH_SCREEN_TIME_OUT);
    }
}
